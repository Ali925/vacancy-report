FROM node:8.12.0-alpine

COPY . .
RUN npm config set unsafe-perm true
RUN npm set progress=false && npm config set depth 0 && npm cache clean --force
RUN npm install

ENV NODE_ENV prod
ENV PORT 3000

EXPOSE  3000

CMD ["node","./bin/www"]