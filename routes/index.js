var express = require('express');
var path = require('path');
var router = express.Router();
const axios = require('axios');
var url = require('url');
const cheerio = require("cheerio");
var mysql  = require('mysql');

// var connection = mysql.createConnection({
//   host     : '5.189.184.224',
//   user     : 'ali925',
//   password : 'q1146666',
//   database : 'report'
// });

router.get('/', function(req, res, next) {
  res.render('main', {layout: false});
});

router.get('/report', function(req, res, next) {
    let portals = ["https://www.jobsearch.az/",  "https://boss.az/vacancies?action=index&controller=vacancies&only_path=true&page=", 
    "https://www.rabota.az/vacancy/search?sortby=3&created=30&request=&page=", "http://www.azerweb.az/en/index.php",
    "https://hh1.az/search/vacancy?L_is_autosearch=false&area=9&clusters=true&currency_code=AZN&enable_snippets=true&page=", 
    "https://azinka.az/jobs"], portalsData = {}, loads = 1;

    var promise = new Promise(function(resolve, reject) {
      portals.forEach(async function(element, index) {
      if(index == 0){
        try {
          let result = await axios.get(element);
          let $ = cheerio.load(result.data);

          portalsData['jobsearch.az'] = [];
          $("table.hotvac > tbody > tr").each((i, el) => {
            if(i){
              let pdata = {};
              pdata.vacancy = $(el).find("td.hotv_text > a.hotv_text").text();
              pdata.company = $(el).find("td.hotv_text:nth-child(2)").text();
              portalsData['jobsearch.az'].push(pdata);
            }
          });
        } catch(err) {
          console.log("jobsearch: ", err);
        }
        
      } else if(index == 1){
        let load = true, page = 1;
        portalsData['boss.az'] = [];

        while(load){
          try{
          let result = await axios.get(element + page + "&type=vacancies");
          let $ = cheerio.load(result.data);

          if($(".results > .results-i").length > 0){
            page++;
            $(".results > .results-i").each((i, el) => {
               let pdata = {};
                pdata.vacancy = $(el).find("h3.results-i-title").text();
                pdata.company = $(el).find("a.results-i-company").text();
                portalsData['boss.az'].push(pdata);
            });
          } else {
            load = false;
            console.log('loads boss.az: ', loads);
            loads++;
            console.log('loads increment boss.az!!: ', loads);
            if(loads == 3)
              resolve();
          }
        } catch(err) {
          console.log("boss,az: ", err);
        } 
        }
      } else if(index == 2){
        let load = true, page = 1;
        portalsData['rabota.az'] = [];

        while(load){
          try{
          let result = await axios.get(element + page);
          let $ = cheerio.load(result.data);

          if($("#vacancy-list > .visitor-vacancy-list > .vvl-one").length > 0){
            page++;
            $("#vacancy-list > .visitor-vacancy-list > .vvl-one").each((i, el) => {
               let pdata = {};
                pdata.vacancy = $(el).find(".vvlo-base > div > a.title- > h2").text();
                pdata.company = $(el).find(".vvlo-base > div span.company- > b").text();
                portalsData['rabota.az'].push(pdata);
            });
          } else {
            load = false;
            console.log('loads rabota.az: ', loads);
            loads++;
            console.log('loads increment rabota.az!!: ', loads);
            if(loads == 3)
              resolve();
          }
        } catch(err){
          console.log('rabota.az: ', err);
        }
        }
      } else if(index == 3){
        try{
        let result = await axios.get(element);
        let $ = cheerio.load(result.data);

        portalsData['azerweb.az'] = [];
        $("body > table:nth-child(4) > tbody > tr > td:nth-child(3) > center:nth-child(5) > table tr").each((i, el) => {
          if($(el).find("td.more").length > 0){
            let pdata = {};
            pdata.company = $(el).find("td.more > b > u").text();
            pdata.vacancy = $(el).find("td.more > a.more").text();
            portalsData['azerweb.az'].push(pdata);
          }
        });
      } catch(err){
        console.log('azerweb: ', err);
      }
      } else if(index == 4){
        // let load = true, page = 0;
        // portalsData['jobs.day.az'] = [];

        // while(load){
        //   try{
        //   let result = await axios.get(element + page);
        //   let $ = cheerio.load(result.data);

        //   if($(".vacancy-serp > .vacancy-serp-item").length > 0){
        //     page++;
        //     $(".vacancy-serp > .vacancy-serp-item").each((i, el) => {
        //        let pdata = {};
        //         pdata.vacancy = $(el).find(".vacancy-serp-item__row_header .vacancy-serp-item__info .resume-search-item__name a").text();
        //         pdata.company = $(el).find(".vacancy-serp-item__meta-info > a.bloko-link_secondary").text();
        //         portalsData['jobs.day.az'].push(pdata);
        //     });
        //   } else {
        //     load = false;
        //     console.log('loads jobs.day.az: ', loads);
        //     loads++;
        //     console.log('loads increment jobs.day.az!!: ', loads);
        //     if(loads == 3)
        //       resolve();
        //   }
        // } catch(err) {
        //   console.log('jobs.day: ', err);
        // }
        // }
      } else if(index == 5){
        let load = true, page = 1;
        portalsData['azinka.az'] = [];

        while(load){
          try{
          let url = element;
          if(page > 1){
            url += '/page/' + page;
          }
          let result = await axios.get(url);
          let $ = cheerio.load(result.data);

          if($(".posts-loop-content article.noo_job").length > 0){
            page++;
            $(".posts-loop-content article.noo_job").each((i, el) => {
               let pdata = {};
                pdata.vacancy = $(el).find("div.loop-item-wrap > div.loop-item-content > .loop-item-title > a").text();
                pdata.company = $(el).find("div.loop-item-wrap > div.loop-item-content > .content-meta > .job-company > a").text();
                portalsData['azinka.az'].push(pdata);
            });
          } else {
            load = false;
            // console.log('loads azinka.az: ', loads);
            // loads++;
            // console.log('loads increment azinka.az!!: ', loads);
            // if(loads == 4)
            //   resolve();
          }
        } catch(err) {
          console.log('azinka: ', err);
        }
        }
      }
    });
  });

    promise.then(function(){
      res.render('report', {data: JSON.stringify(portalsData), layout: false});
    });

});

router.get('/maillist', function(req, res, next) {

  let emailData = [];

  // connection.query('SELECT * FROM emails', function (error, results, fields) {
  //   if (error) throw error;

  //   results.forEach(r => {
  //     let d = {};
  //     Object.keys(r).forEach(k => {
  //       d[k] = r[k];
  //     });
  //     emailData.push(d);
  //   });

  //   console.log('emails: ', emailData);
      
  //   res.render('maillist', {data: JSON.stringify(emailData), layout: false});
  // });

});

function getMailList(){

  

}

getMailList();

setInterval(getMailList, 3600000);

module.exports = router;
