/* DOCUMENT INFORMATION
  - Document: Glorri Web Project
  - Version:  1.0
  - Client:   Glorri
  - Author:   Azeroglu Emin
*/

$(function() {

    var scrollTalent    =   $('.talent-profile-1');

    if ( scrollTalent.length ) {
        button.talentScroll();
    }

    $('body').on('change','.js-images-view',function(){
        button.imagesView($(this));
    });

    $('body').on('click','.js-images-remove',function(){
        button.imagesRemove($(this));
    });

    /* [ Form Select Box ] */
    $("body").on("click", ".form-select-box .default-value", function() {
        var object = $(this);
        form.selectBox(object);
    });
    $("body").on("click", ".form-select-box .select-option .option", function() {
        var object = $(this);
        form.selectBoxSelect(object);
    });
    /* [ Form Select Box End ] */

    /* [ Form Line Date ] */
    $("body").on("click", ".form-line-date .arrow", function() {
        var object = $(this);
        form.lineDate(object);
    });
    /* [ Form Line Date End ] */

    /* [ Form Image Preview ] */
    $("body").on("change", ".img-content .image-preview", function(e) {
        var object = $(this);
        form.imagePreview(object, e);
    });
    $("body").on("click", ".img-content .img-remove", function() {
        var object = $(this);
        form.imagePreviewRemove(object);
    });
    /* [ Form Image Preview End ] */

    /* [ Form Video Upload ] */
    $('body').on('input', '.js-video-upload-input', function () {
        var object = $(this);
        form.videoUploadInput(object);
    });
    $('body').on('click', '.js-video-add-button', function () {
        var object = $(this);
        form.videoUploadAdd(object);
    });
    $('body').on('click', '.js-video-cancel-button', function () {
        var object = $(this);
        form.videoUploadCancel(object);
    });
    /* [ Form Video Upload End ] */

    /* [ Form Map Upload ] */
    $('body').on('input', '.js-modal-map-upload', function () {
        var object = $(this);
        form.mapUploadInput(object);
    });
    /* [ Form Map Upload End ] */

    /* [ Follow Button ] */
    $('body').on('click', '.js-button-follow', function() {
        var object = $(this);
        button.follow(object);
    });
    /* [ Follow Button End ] */

    /* [ Show Block Button ] */
    $('body').on('click', '.js-show-block-button', function() {
        var object = $(this);
        button.blockOpen(object);
    });
    /* [ Show Block Button End ] */

    /* [ Show Block Button ] */
    $('body').on('click', '.js-form-collapse-button', function() {
        var object = $(this);
        form.collapse(object);
    });
    /* [ Show Block Button End ] */

    /* [ Modal Block ] */
    $('body').on('click', '.js-modal-block', function(e) {
        e.preventDefault();
        var object = $(this);
        button.ModalBlock(object);
    });
//    $('body').on('click', '.box-modal', function(e) {
//        var object = $(this);
//        button.ModalBlockClose(e, 'all');
//    });
    $('body').on('click', '.js-modal-block-close', function(e) {
        var object = $(this);
        button.ModalBlockClose(object, 'button');
    });
    /* [ Modal Block End ] */


    /* [ Form Step ] */
    $('body').on('click', '.js-form-step', function () {
        var object = $(this);
        form.step(object);
    });
    /* [ Form Step End ] */

    /* [ Form Step ] */
    $('body').on('click', '.js-dropdown', function () {
        var object = $(this);
        button.dropdown(object);
    });
    /* [ Form Step End ] */

    /* [ Tab System ] */
    $('body').on('click', '.js-tab-system', function () {
        var object = $(this);
        button.tabSystem(object);
    });
    /* [ Tab System End ] */

    /* [ Bulk System ] */
    $('body').on('click', '.js-bulk-button', function () {
        var object = $(this);
        button.bulk(object);
    });
    /* [ Bulk System End ] */


    /* [ Form New Skill ] */
    $('body').on('click', '.js-add-modal-element-button', function () {
        var object = $(this);
        form.addModalElement(object)
    })
    /* [ Form New Skill End ] */

    /* [ Sub Menu ] */
    $('body').on('click', '.js-sub-menu', function () {
        var object = $(this);
        button.subMenu(object);
    });
    /* [ Sub Menu End ] */

    /* [ Remove Data ] */
    $('body').on('click', '.js-remove-data', function () {
        var object = $(this);
        button.removeData(object);
    });
    /* [ Remove Data End ] */

    /* [ All Checked ] */
    $('body').on('change', '.js-checked-all', function () {
        var object = $(this);
        button.allChecked(object);
    });
    /* [ All Checked End ] */

    /* [ Add Element ] */
    $('body').on('click', '.js-add-input', function () {
        var object = $(this);
        button.addElement(object);
    });
    /* [ Add Element End ] */

    /* [ View ] */
    $('body').on('click', '.js-view', function () {
        var object = $(this);
        button.view(object);
    });
    /* [ View End ] */

    /* [ Add Elemetn ] */
    $('body').on('click', '.js-add-element', function () {
        var object = $(this);
        button.elmentAdd(object);
    })
    /* [ Add Elemetn End ] */

    /* [ Tab Systems ] */
    $('body').on('click', '.js-tap-systems', function () {
        var object = $(this);
        button.tabSystems(object);
    });
    /* [ Tab Systems End ] */

    /* [ App Dropdown ] */
    $('body').on('click', '.app-dropdown', function () {
        var object = $(this);
        button.appDropdown(object);
    });
    /* [ App Dropdown End ] */

    /* [ App Photo View ] */
    $('body').on('change','.app-photo-view',function (e) {
        var object = $(this);
        button.appPhotoView(object, e);
    });
    /* [ App Photo View End ] */

    /* [ App Photo View Remove ] */
    $('body').on('click', '.app-photo-remove', function () {
        var object = $(this);
        button.appPhotoViewRemove(object);
    });
    /* [ App Photo View Remove End ] */

    /* [ App Form Tags ] */
    $('body').on('keyup', '.app-form-tags', function (e){
        var object = $(this);
        form.tags(object, e);
    });
    /* [ App Form Tags End ] */

    /* [ App Form Tags ] */
    $('body').on('click', '.js-add-buttons', function (e){
        var object = $(this);
        button.addButtons(object, e);
    });
    /* [ App Form Tags End ] */

    /* [ Dropdown Menu ] */
    $('body').on('click', '.js-dropdown-menu', function (e){
        e.preventDefault();
        var object = $(this);
        button.dropdownMenu(object);
    });
    /* [ Dropdown Menu End ] */

    /* [ App Form Tags ] */
    $('body').on('click', '.js-dropdown-block', function (e){
        var object = $(this);
        button.dropdownBlock(object, e);
    });
    /* [ App Form Tags End ] */

    /* [ App Form Tags ] */
    $('body').on('click', '.js-rating span.star', function (e){
        var object = $(this);
        button.rating(object, e);
    });
    $('body').on('mouseover', '.js-rating span.star', function (){
        var object = $(this);
        var parent  =   '.js-rating';
        var child   =   '.star';
        var value   =   parseInt(object.data('value'));

        object.parents(parent).find(child).removeClass('hover');

        for (var i = 0; i < value; i++){
            object.parents(parent).find(child+':eq('+i+')').addClass('hover');
        }

    })
    $('body').on('mouseout', '.js-rating span.star', function () {
        var object = $(this);
        var parent  =   '.js-rating';
        var child   =   '.star';
        var value   =   parseInt(object.data('value'));

        object.parents(parent).find(child).removeClass('hover');
    });
    /* [ App Form Tags End ] */

    /* [ Picture Add ] */
    $('body').on('keyup', '.js-picture-add', function (e) {
        button.pictureAdd($(this), e);
    });
    /* [ Picture Add End ] */

    /* Question Type */
    $('body').on('change', '.js-question-type', function () {
        button.questionType($(this));
    });
    /* Question Type End */

    /* Question Edit */
    $('body').on('click', '.js-question-form', function () {
        button.questionEdit($(this));
    })
    /* Question Edit End */
    
    $('body').on('click', '#socialShare > .socialBox', function() {

      var self = $(this);
      var element = $('#socialGallery a');
      var c = 0;

      if (self.hasClass('animate')) {
        return;
      }

      if (!self.hasClass('open')) {

        self.addClass('open');

        TweenMax.staggerTo(element, 0.3, {
            opacity: 1,
            visibility: 'visible'
          },
          0.075);
        TweenMax.staggerTo(element, 0.3, {
            top: -12,
            ease: Cubic.easeOut
          },
          0.075);

        TweenMax.staggerTo(element, 0.2, {
            top: 0,
            delay: 0.1,
            ease: Cubic.easeOut,
            onComplete: function() {
              c++;
              if (c >= element.length) {
                self.removeClass('animate');
              }
            }
          },
          0.075);

        self.addClass('animate');

      } else {

        TweenMax.staggerTo(element, 0.3, {
            opacity: 0,
            onComplete: function() {
              c++;
              if (c >= element.length) {
                self.removeClass('open animate');
                element.css('visibility', 'hidden');
              };
            }
          },
          0.075);
      }
    });

});
