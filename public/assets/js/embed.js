var _API = "https://test.glorri.az/", _translations = [], monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

var domElement = function(selector){
	this.el = selector;
	this.ready = function(callback){
  		this.el.addEventListener('DOMContentLoaded', callback(), false);
  	};
};

var gmb = function (selector){
	var el = new domElement(selector);
	return el; //return the domELement
};

var gmb_embed = function(jobID){
  getTranslateData(function(tran){
    console.log("translations: ", JSON.parse(tran));
    _translations = JSON.parse(tran);
    
    getJobData (jobID, function(data){
     console.log(JSON.parse(data));
     var jobData = JSON.parse(data);
     jobData.postedDate = calcDiffTime(jobData.postedDate);
     jobData.deadline = formatDate(jobData.deadline);
     makeTemplate(jobData);
     renderJobSkills(jobData.jobSkills);
   });
  });
};

function getJobData(id, callback){
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange = function() {
	  if (this.readyState == 4 && this.status == 200) {
	    var jobData = this.responseText;
	    callback(jobData);
	  }
	};
	xhttp.open("GET", _API + "job-service/job-external/" + id, true);
	xhttp.send();
}

function getTranslateData(callback){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var translateData = this.responseText;
      callback(translateData);
    }
  };
  xhttp.open("GET", _API + "translation-service/translations/en", true);
  xhttp.send();
}

function getSkill(id, level, callback){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      callback(this.responseText, level);
    }
  };
  xhttp.open("GET", _API + "tag-service/skill-tag/"+id, true);
  xhttp.send();
}

function translate(sufix, key){
  let index = _translations.findIndex(t => {return (sufix + key) == t.key});
  if(index !== -1){
    return _translations[index].value;
  } else {
    return '';
  }
  
}

function calcDiffTime(date){
     let diffTime = (new Date()).getTime() - (new Date(date)).getTime(), temp = '';

        switch(true){
          case (diffTime/(1000*60*60*24*365) >= 1):
            if(Math.floor(diffTime/(1000*60*60*24*365)) > 1)
              temp = Math.floor(diffTime/(1000*60*60*24*365)) + " years ago";
            else 
              temp = "a year ago";
            break;
          case (diffTime/(1000*60*60*24*30) >= 1):
            if(Math.floor(diffTime/(1000*60*60*24*30)) > 1)
              temp = Math.floor(diffTime/(1000*60*60*24*30)) + " months ago";
            else 
              temp = "a month ago";
            break;
          case (diffTime/(1000*60*60*24*7) >= 1):
            if(Math.floor(diffTime/(1000*60*60*24*7)) > 1)
              temp = Math.floor(diffTime/(1000*60*60*24*7)) + " weeks ago";
            else 
              temp = "a week ago";
            break;
          case (diffTime/(1000*60*60*24) >= 1):
            if(Math.floor(diffTime/(1000*60*60*24)) > 1)
              temp = Math.floor(diffTime/(1000*60*60*24)) + " days ago";
            else 
              temp = "a day ago";
            break;
          case (diffTime/(1000*60*60) >= 1):
            if(Math.floor(diffTime/(1000*60*60)) > 1)
              temp = Math.floor(diffTime/(1000*60*60)) + " hours ago";
            else 
              temp = "an hour ago";
            break;
          case (diffTime/(1000*60) >= 1):
            if(Math.floor(diffTime/(1000*60)) > 1)
              temp = Math.floor(diffTime/(1000*60)) + " minutes ago";
            else 
              temp = "a minute ago";
            break;
          default:
            temp = "just now";
            break;
        }
        
        return temp;
}

function formatDate(date){
  var temp = "", datetime = new Date(date);
  temp = monthNames[datetime.getMonth()] + " " + datetime.getDate() + ", " + datetime.getFullYear();
  return temp;
}

function renderJobSkills(skills){
 skills.forEach((skill, index) => {
   getSkill(skill.skillTagId, skill.skillLevel, function(skillData, level){
     var skillD = JSON.parse(skillData);
     var parentNode = document.createElement("LI");
     var childNode1 = document.createElement("SPAN");
     var childNode2 = document.createElement("SPAN");
     var childNode3 = document.createElement("SPAN");
     childNode1.appendChild(document.createTextNode(skillD.name));
     childNode2.appendChild(document.createTextNode(level+'/5'));
     childNode3.className = 'clear;'
     parentNode.appendChild(childNode1);
     parentNode.appendChild(childNode2);
     parentNode.appendChild(childNode3);
     document.getElementById("gmbJobSkills").appendChild(parentNode);
   });
 }); 
}

function makeTemplate(data){
	var temp = '<div class="gmb_embed_job_box"><div class="gmb_box_header"><h2>'+data.title+'</h2>' +
               '<span>'+translate("cities.", data.city)+'</span> • <span>'+data.postedDate+'</span><small>'+translate("job-types.", data.type)+'</small></div><div class="gmb_box_body">' +
      		   '<section><h3>Description</h3>' + data.description +
      		   '</section><section><h3>Skills</h3>' + data.remarks +
       		   '<ul id="gmbJobSkills"></ul></section>' +
      		   '<section><h3>Details</h3><ul>' +
           		'<li><span>Salary</span><span>'+data.paymentMin+' - '+data.paymentMax+' '+translate("currencies.", data.paymentCurrency)+' / '+translate("payment-method.", data.paymentMethod)+'</span> <span class="clear"></span></li>' +
           		'<li><span>Deadline</span><span>'+data.deadline+'</span> <span class="clear"></span></li>' +
           		'<li><span>Experience</span><span>'+translate("experiences.", data.experience)+'</span> <span class="clear"></span></li>' +
           		'<li><span>Industry</span><span>'+translate("industries.", data.industry)+'</span> <span class="clear"></span></li>' +
              '<li><span>Category</span><span>'+translate("industries.", data.subIndustry)+'</span> <span class="clear"></span></li>' +
           		'<li><span>Education</span><span>'+translate("educations.", data.education)+'</span> <span class="clear"></span></li>' +
           		'<li><span>Career level</span><span>'+translate("career-levels.", data.careerLevel)+'</span> <span class="clear"></span></li>' +
         		'</ul></section><section><a class="gmb_apply_btn" href="https://glorri.az/company/job/'+data.id+'/apply?source=Company" target="_blank">Apply</a></section>' +
             '</div></div><div class="gmb_box_footer"><p>Hiring with</p><div class="gmb_footer_box_image"><a href="https://glorri.az" target="_blank"><img src="https://glorri.az/assets/img/glorri-logo_2.svg"></a></div></div>';

    document.getElementById("gmb_embed_job").innerHTML = temp;

    var head  = document.getElementsByTagName('head')[0];
    var link  = document.createElement('link');
    link.rel  = 'stylesheet';
    link.type = 'text/css';
    link.href = 'https://glorri.az/assets/css/embed.min.css';
    link.media = 'all';
    head.appendChild(link);
}