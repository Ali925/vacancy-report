var activeCompanyJobs = [], jobapi= "https://test.glorri.az/job-service/", tagapi="https://test.glorri.az/tag-service/", 
fileapi="https://test.glorri.az/file-service/", talentapi="https://test.glorri.az/talent-service/", userapi = "https://test.glorri.az/user-service/",
transapi = "https://test.glorri.az/translation-service/", appapi = "https://test.glorri.az/application-service/", translations = [], 
jobs = [], currentJob;

var jobLevels = {
	"beginner": 1,
	"intermediate": 2,
	"advanced": 3,
	"expert": 4
};

$(document).ready(function(){
	var companyId = $("#contentBlock").data("id"), jobsCount = 0;

	/* Scrollbar Active */
    $('.scrollbar-inner, .scrollbar-macosx').scrollbar({
        'autoScrollSize': true,
    });

    /* Scroll Top */
    $(window).on('scroll', function () {
        const scrollTop = $(this).scrollTop();
        Events.ScrollTop(scrollTop, $(this));
    });

    $.ajax({
    	method: 'POST',
    	url: userapi + 'auth/job-token/'+ companyId
    }).then(function(res){
    	window.localStorage.token = res;
    	$.ajax({
    		method: 'GET',
    		url: jobapi + "/company/"+companyId+"/jobs-external",
    		headers: {
                'X-Auth-Token': res
            }
    	})
    	.then((data) => {
    		console.log('jobs: ', data);
    		getCompanyJobs(data);
    	}, (err) => {
    		console.log('error: ', err);
    	});
    }, function(err){
    	console.log(err);
    });

    $.get(fileapi + "/file/"+$(".wallpaper")[0].dataset.info+"/url")
    	.then((data) => {
    		console.log("data: ", data, $(".wallpaper").find(".bem-photo__src"));
    		$(".wallpaper").find(".bem-photo__src").css("background-image", "url("+data+")");
    	}, (err) => {
    		console.log(err);
    	});

    $.get(fileapi + "/file/"+$(".logo")[0].dataset.info+"/url")
    	.then((data) => {
    		console.log("data: ", data, $(".logo").find(".bem-photo__src"));
    		$(".logo").find(".bem-photo__src").css("background-image", "url("+data+")");
    		$(".logo").find("span.icon").hide();
    	}, (err) => {
    		console.log(err);
    	});

    $.get(transapi+"/translations/en")
		.then(function(data){
			console.log("translations: ", data);
			translations = data;

			translations.forEach(t=>{
	           				if($("#industryInfo").data("info") == t.key){
	           					$("#industryInfo").text(t.value);
	           				}
	         });
		}, function(err){
			console.log(err);
		});

	$.get(talentapi+"company/"+companyId+"/followers")
		.then(function(data){
			console.log("followers: ", data);
			$("#followersCount").text(data);
		}, function(err){
			console.log(err);
		});

	var jobsTemp = '', jobsLength = 0;
	function getCompanyJobs(data){
		$("#activeJobsCount").text(data.length);
           	$("#jobsCount").text(data.length);
           	jobsCount = data.length;
           	jobs = data;
           	data.forEach(el => {
           		$.get(jobapi + "job-external/"+el)
	           		.then(function(data){
	           			console.log("job: ", data);
	           			var index = jobs.findIndex(j => j == data.id);
	           			jobs[index] = data;
	           			jobsLength++;
	           			if(jobsLength == jobsCount){
	           				jobs.forEach(data => {
		           				translations.forEach(t=>{
			           				if("countries."+data.country == t.key){
			           					data.country = t.value;
			           				} else if("cities."+data.city == t.key){
			           					data.city = t.value;
			           				} else if("job-types."+data.type == t.key){
			           					data.jobType = t.value;
			           				}
			           			});

			           			var timeDiff = (new Date()).getTime() - (new Date(data.postedDate)).getTime(), time = '';

			           			if(timeDiff > (1000 * 3600 * 24 * 30 * 12)){
			           				time = Math.round(timeDiff / (1000 * 3600 * 24 * 30 * 12)).toString();
			           				if(time > 1)
			           					time += " years ago";
			           				else
			           					time = "a year ago"; 
			           			} else if(timeDiff > (1000 * 3600 * 24 * 30)){
			           				time = Math.round(timeDiff / (1000 * 3600 * 24 * 30)).toString();
			           				if(time > 1)
			           					time += " months ago";
			           				else
			           					time = "a month ago"; 
			           			} else if(timeDiff > (1000 * 3600 * 24)){
			           				time = Math.round(timeDiff / (1000 * 3600 * 24)).toString();
			           				if(time > 1)
			           					time += " days ago";
			           				else
			           					time = "a day ago"; 
			           			} else if(timeDiff > (1000 * 3600)){
			           				time = Math.round(timeDiff / (1000 * 3600)).toString();
			           				if(time > 1)
			           					time += " hours ago";
			           				else
			           					time = "an hour ago"; 
			           			} else if(timeDiff > (1000 * 60)){
			           				time = Math.round(timeDiff / (1000 * 60)).toString();
			           				if(time > 1)
			           					time += " minutes ago";
			           				else
			           					time = "a minute ago"; 
			           			} else{
			           				time = Math.round(timeDiff / (1000)).toString();
			           				if(time > 10)
			           					time += " seconds ago";
			           				else
			           					time = "just now"; 
			           			}
			           			var companyName = $("#companyName").text();
			           			// jobsTemp += '<div class="jobs-list-item" id="job'+data.id+'" onclick="selectJob(\''+data.id+'\')">'+
			           			// 			'<p>'+data.title+'</p><span>'+companyName+'</span>'+
			           			// 			'<i class="float-right">'+time+'</i>'+
			           			// 			'<div class="clearfix"></div></div>';

			           			jobsTemp += '<div class="bem-openings__aside__item" id="job'+data.id+'" onclick="selectJob(\''+data.id+'\')">' +
		                                    '<div class="name">'+data.title+'</div><div class="subname"><a href="#">'+companyName+'</a>' +
		                                    '</div><div class="location">'+data.city+'</div><div class="time">'+time+'</div></div>';
		           			});

	           				$("#jobsContent").html(jobsTemp);
	           				$("#jobsContent > .bem-openings__aside__item:first-child").click();
	           			}
	           		}, function(err){
	           			console.log(err);
	           		});


           	});
	}
});

function selectJob(id){
	var el = $("#job"+id);
	currentJob = id;
	$(".bem-openings__aside__item").removeClass("bem-openings__aside__item--active");
	el.addClass("bem-openings__aside__item--active");

	$.get(appapi + 'applications/'+id+'/count').then(function(data){
		if(isNaN(data)){
			$("#jobDescApps").text('0');
		} else {
			$("#jobDescApps").text(data);
		}
	}, function(err){
		console.log(err);
	});
	let data = jobs.find(j => j.id == id);
	// $.get(jobapi + "job/"+id)
	// 	.then(function(data){
           	console.log("selected job: ", data);
           	translations.forEach(t=>{
	           				if("countries."+data.country == t.key){
	           					data.country = t.value;
	           				} else if("cities."+data.city == t.key){
	           					data.city = t.value;
	           				} else if("job-types."+data.type == t.key){
	           					data.jobType = t.value;
	           				} else if('experiences.' + data.experience == t.key){
	           					data.experience = t.value;
	           				} else if('career-levels.' + data.careerLevel == t.key){
	           					data.careerLevel = t.value;
	           				} else if('industries.' + data.industry == t.key){
	           					data.industry = t.value;
	           				} else if('industries.' + data.subIndustry == t.key){
	           					data.subIndustry = t.value;
	           				} else if('currencies.' + data.paymentCurrency == t.key){
	           					data.paymentCurrency = t.value;
	           				} else if('payment-method.' + data.paymentMethod == t.key){
	           					data.paymentMethod = t.value;
	           				}

	           				data.jobSkills.forEach((el, index) => {
	           					if("skill-levels." + el.skillLevel == t.key){
	           						data.jobSkills[index].skillLevelName = t.value;
	           					}
	           				});
	           			});

	           			var timeDiff = (new Date()).getTime() - (new Date(data.postedDate)).getTime(), time = '';

	           			if(timeDiff > (1000 * 3600 * 24 * 30 * 12)){
	           				time = Math.round(timeDiff / (1000 * 3600 * 24 * 30 * 12)).toString();
	           				if(time > 1)
	           					time += " years ago";
	           				else
	           					time = "a year ago"; 
	           			} else if(timeDiff > (1000 * 3600 * 24 * 30)){
	           				time = Math.round(timeDiff / (1000 * 3600 * 24 * 30)).toString();
	           				if(time > 1)
	           					time += " months ago";
	           				else
	           					time = "a month ago"; 
	           			} else if(timeDiff > (1000 * 3600 * 24)){
	           				time = Math.round(timeDiff / (1000 * 3600 * 24)).toString();
	           				if(time > 1)
	           					time += " days ago";
	           				else
	           					time = "a day ago"; 
	           			} else if(timeDiff > (1000 * 3600)){
	           				time = Math.round(timeDiff / (1000 * 3600)).toString();
	           				if(time > 1)
	           					time += " hours ago";
	           				else
	           					time = "an hour ago"; 
	           			} else if(timeDiff > (1000 * 60)){
	           				time = Math.round(timeDiff / (1000 * 60)).toString();
	           				if(time > 1)
	           					time += " minutes ago";
	           				else
	           					time = "a minute ago"; 
	           			} else{
	           				time = Math.round(timeDiff / (1000)).toString();
	           				if(time > 10)
	           					time += " seconds ago";
	           				else
	           					time = "just now"; 
	           			}
           	var job = data;
           	if(job.coverImage){
	           	$.get(fileapi+"/file/"+job.coverImage+"/url").then(function(file){
	           		var temp = '<figure class="bem-photo wallpaper"><div class="bem-photo__src bem-photo__src--cover"' + 
	           					'style="background-image: url(\''+file+'\');"></div></figure>';
		           	$("#company_wallpaper").html(temp);

		           	}, function(err){
		           	console.log(err);
		           }
		         );
            } else {
            	$("#company_wallpaper > figure").remove();
            }

            $(".bem-skill__content").html('');
            let skills = {};
            job.jobSkills.forEach(el => {
            	var temp = '';
            	$.get(tagapi + "skill-tag/" + el.skillTagId).then(function(data){
            		//console.log(data);
            		skills[data.id] = data;
            		if(Object.keys(skills).length == job.jobSkills.length){
            			job.jobSkills.forEach(jb => {
            				let data = skills[jb.skillTagId];
	            			if(jb.skillTagId == data.id){
	            				temp += '<div class="bem-skill__item"><div class="title">'+data.name+'</div><div class="options">';

	            				for(var i=1;i<=5;i++){
	            					if(parseFloat(jb.skillLevel) >= i || (i - parseFloat(jb.skillLevel) < 1)){
	            						if(parseFloat(jb.skillLevel) >= i){
	            							temp += '<span class="option"><span class="option--active" style="width: 100%;height:100%;display:block;"></span></span>';
	            						} else {
	            							var per = (1 - (i - parseFloat(jb.skillLevel)))*100 + '%';
	            							 temp += '<span class="option"><span class="option--active" style="width: '+per+';height:100%;display:block;"></span></span>';
	            						}
	            					}
	            					else
	            						temp += '<span class="option"></span>';
	            				}

	            				temp += '</div></div>';

	            			}
	            		});

	            		$(".bem-skill__content").append(temp);
            		}
            		
            	}, function(err){
            		console.log(err);
            	});

            });
            $(".bem-benefits__content").html('');
            if(job.benefits.length){
            	$("#benSection").show();
	            job.benefits.forEach(el => {
	            	var temp = '';
	            	
	            	$.get(tagapi+"/benefit-tag/"+el.benefitTagId).then(function(data){
	            		//console.log("benefit: ", data);

	            		temp = '<div class="bem-benefits__item">'+data.name+'</div>';
	            		$(".bem-benefits__content").append(temp);
	            	}, function(err){
	            		console.log(err);
	            	});

	            });
        	} else {
        		$("#benSection").hide();
        	}

            if(job.department){
	            $.get(tagapi+"/department/"+job.department).then(function(data){
	            		//console.log("benefit: ", data);
	            		$("#jobDep").show();
	            		$("#jobDep > small").text(data.name);
	            	}, function(err){
	            		console.log(err);
	            	});
        	} else {
        		$("#jobDep").hide();
        	}

        	if(job.deadline){
        		$("#jobDedl").show();
        		var temp = '';
        		var date = new Date(job.deadline);
        		temp = date.getDate() + "." + (date.getMonth()+1) + "." + date.getFullYear();
        		$("#jobDedl>small").text(temp);
        	} else {
        		$("#jobDedl").hide();
        	}

            $("#jobTitle").text(job.title);
            $("#jobDescCity").text(job.city);
            $("#jobDescViews").text(job.viewCount);
            $("#jobDescText").html(job.description);
            $("#reqText").html(job.remarks);
            if(job.experience)
            	$("#jobExp>small").text(job.experience);
            else
            	$("#jobExp").remove();
            if(job.careerLevel)
            	$("#jobCarlev>small").text(job.careerLevel);
            else 
            	$("#jobCarlev").remove();
            if(job.jobType)
            	$("#jobEmptype>small").text(job.jobType);
            else
            	$("#jobEmptype").remove();
            $("#jobType").text(job.jobType);
            $("#jobInd>small").text(job.industry);
            if(job.subIndustry)
            	$("#jobCat>small").text(job.subIndustry);
            else
            	$("#jobCat").remove();

            $("#sendButton").attr('href', 'mailto:?subject='+job.title+'&body=https://glorri.az/company/job/'+job.id);
            if(job.paymentMin)
            	$("#jobSal>small").text(job.paymentMin + " " + job.paymentCurrency + " / " + job.paymentMethod);
            else
            	$("#jobSal").remove();
            if(job.isVisaSponsored)
            	$("#jobVisa>small").text("Yes");
            else
            	$("#jobVisa>small").text("No");
           // }, function(err){
           // 	console.log(err);
           // });
}

function share(type){
			var link = '';
			switch(type){
				case "fb":
					link = "https://www.facebook.com/sharer/sharer.php?u=https://glorri.az/company/job/"+currentJob+"&t=Share on Facebook";
					break;
				case "tw":
					link = "https://twitter.com/share?url=https://glorri.az/company/job/"+currentJob;
					break;
				case "ln":
					link = "https://www.linkedin.com/shareArticle?mini=true&url=https://glorri.az/company/job/"+currentJob+"&title=Share on Linkedin";
					break;
			}
			let newwindow=window.open(link,'Share on Facebook','height=500,width=500,top=100,left=100,resizable');
            if (window.focus) {newwindow.focus()}
}

function goToLoginApply(){
	window.open("https://bem.glorri.az/job/"+currentJob+"/apply", '_blank');
}

function goToApply(){
	window.open("https://glorri.az/company/job/" + currentJob + "/apply", '_blank');
}