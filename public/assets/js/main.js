var showSubscribe = false, lang = $("body")[0].className.split("-")[0], translations = {}, api = "https://test.glorri.az/user-service/";
$(document).ready(function(){
	var length = $(".main-about-staff").length, first = 1, last = 2, currentFeatureBlock = 1, videoLink = '';
    if(lang != "en" && lang != "az" && lang != "ru")
        lang = "en";
		
		if(window.location.hash == "#subscribe")
			showModal("subscribeModal");
		
		setTimeout(function(){
			if(window.location.href.includes("recruiter")){
				var hash = window.location.hash;
				switch(hash){
					case '#brand':
						$("#fet1 > a").click();
						break;
					case '#hiringteam':
						$("#fet2 > a").click();
						break;
					case '#advertise':
						$("#fet3 > a").click();
						break;
					case '#managetalents':
						$("#fet4 > a").click();
						break;
					case '#test&verify':
						$("#fet5 > a").click();
						break;
					case '#decisions':
						$("#fet6 > a").click();
						break;
				}
			}	
		}, 1550);
	  
	
		$.get("../locales/"+lang+".json").then(function(data){
        //console.log(data);
        translations = data;             
    });
    
    $(".header-top-mob .custom-lang-select > div[data-id='"+lang+"']").hide();
    
    $(".custom-lang-select>.select-items>div[data-id="+lang+"]").addClass("selected");
    $(".custom-lang-select>.select-selected").each(function(){
        $(this).text($(".custom-lang-select>.select-items>div[data-id="+lang+"]")[0].innerHTML);
    });
    
	$(".main-about-staff-left").click(function(){
		if(first > 1){
			first--;
			last--;
			$(".main-about-staff:nth-child("+(first+1)+")").show().addClass("first-el");
			$(".main-about-staff:nth-child("+(last+1)+")").addClass("last-el").removeClass("first-el");
			$(".main-about-staff:nth-child("+(last+2)+")").hide().removeClass("last-el");
		} else{
			first = length-1;
			last = length;
			$(".main-about-staff").removeClass("first-el").removeClass("last-el");
			$(".main-about-staff:nth-child("+first+")").hide();
			$(".main-about-staff:nth-child("+(last)+")").show().addClass("first-el");
			$(".main-about-staff:nth-child("+(last+1)+")").show().addClass("last-el");
		}
	});
	
	$(".main-about-staff-right").click(function(){
		if(last < length){
			$(".main-about-staff:nth-child("+(first+1)+")").hide().removeClass("first-el");
			$(".main-about-staff:nth-child("+(last+1)+")").removeClass("last-el").addClass("first-el");
			$(".main-about-staff:nth-child("+(last+2)+")").show().addClass("last-el");
			first++;
			last++;
		} else{
			first = 1;
			last = 2;
			$(".main-about-staff").removeClass("first-el").removeClass("last-el");
			$(".main-about-staff:nth-child("+(first+1)+")").show().addClass("first-el");
			$(".main-about-staff:nth-child("+(last+1)+")").show().addClass("last-el");
			$(".main-about-staff:nth-child("+(last+2)+")").hide();
		}
	});
	
	$(".header-top-mob > a").click(function(){
		$(".header-top-mob > nav").show();
	});
	
	$(".close-btn").click(function(){
		$(".header-top-mob > nav").hide();
	});
	
	$(".show-leftnav").click(function(){
		if($(this).find("i").hasClass("fa-long-arrow-left")){
			$(this).find("i").removeClass("fa-long-arrow-left");
			$(this).find("i").addClass("fa-long-arrow-right");
			$(".fmain-nav").hide();
			$("#fetBl" + currentFeatureBlock).show();
		} else {
			$(this).find("i").addClass("fa-long-arrow-left");
			$(this).find("i").removeClass("fa-long-arrow-right");
			$(".fmain-nav").show();
			$(".fmain-content").hide();
		}
	});
	
	$(".fmain-nav li > a").click(function(){
		var id = $(this).parent().attr("id").substr(3);
		currentFeatureBlock = id;
			$(".fmain-nav li").removeClass("active");
			$(this).parent().addClass("active");
			$(".fmain-content").hide();		
			if($(window).width() > 1188){
				$("#fetBl"+id).show();
			}
			if($(window).width() <= 1188){
				$(".show-leftnav").click();
			}
	});
	
	$(".faq-box").click(function(){
		if($(this).find("a").css("display") == "none"){
			var key = false;
			if($(this).hasClass("active"))
				key = true;

			$(".faq-box").removeClass("active");
			if(!key)
				$(this).addClass("active");
		}
	});
	
	$(".faq-box > a").click(function(){
		var key = false;
		if($(this).parent().hasClass("active"))
			key = true;
		
		$(".faq-box").removeClass("active");
		if(!key)
			$(this).parent().addClass("active");
	});
	
	$(".modal-close-btn").click(function(){
        if($(this).parent().parent().parent()[0].id == "videoPromoModal"){
            var videoLink = "";
            $("#videoPromoModal .modal-window-text > iframe").attr("src", videoLink);
        }
		$(".modal").fadeOut({duration: 400, easing: "swing"});
		$(".modal-backdrop").hide();
		$("body").removeClass("modal-open");
		$("html").removeClass("modal-open");
	});
	
	$(".select-selected").click(function(){
		$(this).next().toggle();
	});
	
	$(window).click(function($event){
		//console.log($event.target.className);
		if($event.target.className != "select-selected")
			$(".select-items").hide();
	});
	
	$(".select-items > div").click(function(){
		var value = $(this).data("id");
		$(".select-selected").text($(this).text());
		$(".select-items > div").removeClass("selected");
		$(this).addClass("selected");
		$(".custom-lang-select > select").val(value);
		$(".select-items").hide();
	});
	
	$(".input-effect input").focusout(function(){
			if($(this).val() != ""){
				$(this).addClass("has-content");
			}else{
				$(this).removeClass("has-content");
			}
		});
	
	$(".input-effect label").click(function(){
		$(this).prev().focus();
	});
	
	$(".main-table-head-btns > a").click(function(){
		$(".main-table-head-btns > a").removeClass("active");
		$(this).addClass("active");
	});
    
    $(".watch-btn-icon").click(function(){
        var videoLink = "https://www.youtube-nocookie.com/embed/WOPIQxkPlQQ?controls=1&autoplay=1";
        $("#videoPromoModal .modal-window-text > iframe").attr("src", videoLink);
        showModal("videoPromoModal");
    });
    
    $(".subscribe_btn").click(function(){
        showSubscribe = true;
        $('.subscribe_on').toggleClass('on');
        $('.content').toggleClass('opacity');
        setTimeout(function(){showSubscribe = false;}, 500);
    });
    
    $(".subscribe-close-btn").click(function(){
			$(".subscribe_on").removeClass("on");
			$(".btn_subscribe_on").show();
			$('.content').toggleClass('opacity');
		});
    
    if($(document).scrollTop() > 0){
         $(".scroll-top").fadeIn({'easing': 'linear'});  
    } else {
         $(".scroll-top").fadeOut({'easing': 'linear'});
    }
	
		if($(document).scrollTop() > 200){
         $(".header-top").addClass("toBottom"); 
    } else {
           $(".header-top").removeClass("toBottom"); 
    }
    
    $(window).scroll(function(){
        //console.log('aa: ', $("body").scrollTop());
        if($(document).scrollTop() > 0){
           $(".header-top").addClass("toBottom"); 
        } else {
           $(".header-top").removeClass("toBottom"); 
        }
			
				if($(document).scrollTop() > 200){
           $(".scroll-top").fadeIn({'easing': 'linear'}); 
        } else {
           $(".scroll-top").fadeOut({'easing': 'linear'});
        }
    });
	
		$(".scroll-top").click(function(){
			window.scrollTo(0, 0);
		});

    $(".header-slide-btn").mouseover(function(){
        var num = parseInt($(this).data("dot")) - 1;
        $(".header-slider").trigger('to.owl.carousel', num, 1000);
    });
    $(".resume-slide-btn").mouseover(function(){
        var num = parseInt($(this).data("dot")) - 1;
        $(".resume-items").trigger('to.owl.carousel', num, 1000);
    });
    
    $(".resume-item-btn").click(function(){
        var videoLink = "";
				switch(lang){
						case "az": 
								videoLink = "https://www.youtube-nocookie.com/embed/gGS3FTZvj3M?controls=1&autoplay=1";
								$("#video").attr("src", videoLink);
								break;
						case "en":
								videoLink = "https://www.youtube-nocookie.com/embed/FF4VL2XT-bE?controls=1&autoplay=1";
								$("#video").attr("src", videoLink);
								break;
						case "ru":
								videoLink = "https://www.youtube-nocookie.com/embed/_LGygm6VGBc?controls=1&autoplay=1"; 
								$("#video").attr("src", videoLink);
								break;
				}
        $("#videoPromoModal .modal-window-text > iframe").attr("src", videoLink);
        showModal("videoPromoModal");
    });
    
    $(".fViewMore").click(function(){
        console.log($(this).text());
        if($(this).text() == translations["rec_more_btn"]){
            $(this).parent().find(".hidden-item").css("display", "list-item");
            $(this).text(translations["rec_less_btn"]);
        } else {
             $(this).parent().find(".hidden-item").css("display", "none");
            $(this).text(translations["rec_more_btn"]);
        }
    });
    
    $(".subscribe_form .submit").click(function(){
        setTimeout(function(){
            if(!$(".subscribe_form").hasClass("invalid-form")){
                var data = {};
								data.subscribe = {};
                data.subscribe.email = $(".subscribe_form input[name='email']").val();
                data.subscribe.role = $(".subscribe_form input[name='role']").val();
								data.message = {};
                                data.message.lang = lang;
								data.message.header = translations["email_sub_header"];
								data.message.title = translations["email_sub_title"];
								data.message.message = translations["email_sub_message"];
								data.message.btn = translations["email_sub_btn"];
								data.message.footer_faq = translations["email_sub_footer_faq"];
								data.message.footer_faq_1 = translations["email_sub_footer_faq_1"];
								data.message.footer_faq_2 = translations["email_sub_footer_faq_2"];
                                data.message.footer_faq_3 = translations["email_demo_footer_faq_3"];
                                data.message.footer_contact = translations["email_demo_footer_contactus"];
								data.message.footer_cheers = translations["email_sub_footer_cheers"];
								data.message.footer_glorri_1 = translations["email_sub_footer_glorri_1"];
								data.message.footer_glorri_2 = translations["email_sub_footer_glorri_2"];
                $.ajax({
                    method: "POST",
                    url: api + "subscribe",
                    data: data
                }).then(function(res){
                    console.log(res);
                    var text = translations["success_subscribe"];
                    $(".notification").attr("class", "notification clearfix");
                    $(".noti-text").text(text);
                    $(".notification").addClass("success").addClass("on");
                    $("#subscribeModal .modal-close-btn").click();
                    $(".btn_subscribe_on").show();
                    $(".content").removeClass("opacity");
                    $(".subscribe_form input[name='email']").val("");
                    $(".subscribe_form input[name='role']").prop("checked", false);
                    setTimeout(function(){
                        $(".notification").removeClass("on");
                        setTimeout(function(){
                            $(".notification").attr("class", "notification clearfix");
                        }, 500);
                    }, 4000);
                }, function(err){
                    console.log(err);
                    if(err.responseJSON && err.responseJSON.message.indexOf("duplicate key error") != -1){
                        var text = translations["err_email2"];
                        $(".notification").attr("class", "notification clearfix");
                        $(".noti-text").text(text);
                        $(".notification").addClass("info").addClass("on");
                        setTimeout(function(){
                            $(".notification").removeClass("on");
                            setTimeout(function(){
                                $(".notification").attr("class", "notification clearfix");
                            }, 500);
                        }, 4000);
                    } else{
                        var text = translations["err_server1"];
                        $(".notification").attr("class", "notification clearfix");
                        $(".noti-text").text(text);
                        $(".notification").addClass("error").addClass("on");
                        $("#subscribeModal .modal-close-btn").click();
                        $(".btn_subscribe_on").show();
                        $(".content").removeClass("opacity");
                        $(".subscribe_form input[name='email']").val("");
                        $(".subscribe_form input[name='role']").prop("checked", false);
                        setTimeout(function(){
                            $(".notification").removeClass("on");
                            setTimeout(function(){
                                $(".notification").attr("class", "notification clearfix");
                            }, 500);
                        }, 4000);
                    }
                });
            }
        }, 100);
    });
    
    $(".schedule_form .submit").click(function(){
        setTimeout(function(){
            if(!$(".schedule_form").hasClass("invalid-form")){
                var data = {};
								data.demo = {};
                data.demo.name = {
                    first: $(".schedule_form input[name='name']").val(),
                    last: $(".schedule_form input[name='surname']").val()
                };
                data.demo.email = $(".schedule_form input[name='email']").val();
                data.demo.company = $(".schedule_form input[name='company']").val();
                data.demo.jobTitle = $(".schedule_form input[name='job']").val();
                data.demo.phone = $(".schedule_form input[name='phone']").val();
								data.message = {};
                                data.message.lang = lang;
								data.message.header = translations["email_demo_header"];
								data.message.title = translations["email_demo_title"];
								data.message.message = translations["email_demo_message"];
								data.message.btn = translations["email_demo_btn"];
								data.message.footer_faq = translations["email_demo_footer_faq"];
								data.message.footer_faq_1 = translations["email_demo_footer_faq_1"];
								data.message.footer_faq_2 = translations["email_demo_footer_faq_2"];
                                data.message.footer_faq_3 = translations["email_demo_footer_faq_3"];
                                data.message.footer_contact = translations["email_demo_footer_contactus"];
								data.message.footer_cheers = translations["email_demo_footer_cheers"];
								data.message.footer_glorri_1 = translations["email_demo_footer_glorri_1"];
								data.message.footer_glorri_2 = translations["email_demo_footer_glorri_2"];
                console.log("schedule data: ", data);
                $.ajax({
                    method: "POST",
                    url: api + "request-demo",
                    data: data
                }).then(function(res){
                    console.log(res);
                    var text = translations["success_request_demo"];
                    $(".notification").attr("class", "notification clearfix");
                    $(".noti-text").text(text);
                    $("#scheduleModal .modal-close-btn").click();
                    $(".notification").addClass("success").addClass("on");
                    $(".schedule_form input[name='email']").val("");
                    $(".schedule_form input[name='name']").val("");
                    $(".schedule_form input[name='surname']").val("");
                    $(".schedule_form input[name='company']").val("");
                    $(".schedule_form input[name='job']").val("");
                    $(".schedule_form input[name='phone']").val("");
                    setTimeout(function(){
                        $(".notification").removeClass("on");
                        setTimeout(function(){
                            $(".notification").attr("class", "notification clearfix");
                        }, 500);
                    }, 4000);
                }, function(err){
                    console.log(err);
                    if(err.responseJSON && err.responseJSON.message.indexOf("duplicate key error") != -1){
                        var text = translations["err_email2"];
                        $(".notification").attr("class", "notification clearfix");
                        $(".noti-text").text(text);
                        $(".notification").addClass("info").addClass("on");
                        setTimeout(function(){
                            $(".notification").removeClass("on");
                            setTimeout(function(){
                                $(".notification").attr("class", "notification clearfix");
                            }, 500);
                        }, 4000);
                    } else{
                        var text = translations["err_server2"];
                        $(".notification").attr("class", "notification clearfix");
                        $(".noti-text").text(text);
                        $("#scheduleModal .modal-close-btn").click();
                        $(".notification").addClass("error").addClass("on");
                        $(".schedule_form input[name='email']").val("");
                        $(".schedule_form input[name='name']").val("");
                        $(".schedule_form input[name='surname']").val("");
                        $(".schedule_form input[name='company']").val("");
                        $(".schedule_form input[name='job']").val("");
                        $(".schedule_form input[name='phone']").val("");
                        setTimeout(function(){
                            $(".notification").removeClass("on");
                            setTimeout(function(){
                                $(".notification").attr("class", "notification clearfix");
                            }, 500);
                        }, 4000);
                    }
                });
            }
        }, 100);
    });

    $(".header-content-form .submit").click(function(){
        $("#subscribeModal input[name='email']").val($(".header-content-form input[name='email']").val());
        showModal("subscribeModal");

    });

		$(".home_subscribe_form .submit").click(function(){
        setTimeout(function(){
            if(!$(".home_subscribe_form").hasClass("invalid-form")){
                var data = {};
                data.email = $(".home_subscribe_form input[name='email']").val();
                data.role = $(".home_subscribe_form input[name='role']").val();
                $.ajax({
                    method: "POST",
                    url: api + "subscribe",
                    data: data
                }).then(function(res){
                    console.log(res);
                    var text = translations["success_subscribe"];
                    $(".notification").attr("class", "notification clearfix");
                    $(".noti-text").text(text);
                    $(".notification").addClass("success").addClass("on");
                    $("#homeSubscribeRole .modal-close-btn").click();
                    $(".btn_subscribe_on").show();
                    $(".content").removeClass("opacity");
                    $(".subscribe_form input[name='email']").val("");
                    $(".subscribe_form input[name='role']").prop("checked", false);
                    setTimeout(function(){
                        $(".notification").removeClass("on");
                        setTimeout(function(){
                            $(".notification").attr("class", "notification clearfix");
                        }, 500);
                    }, 4000);
                }, function(err){
                    console.log(err);
                    if(err.responseJSON && err.responseJSON.message.indexOf("duplicate key error") != -1){
                        var text = translations["err_email2"];
                        $(".notification").attr("class", "notification clearfix");
                        $(".noti-text").text(text);
                        $(".notification").addClass("info").addClass("on");
												$("#homeSubscribeRole .modal-close-btn").click();
                        setTimeout(function(){
                            $(".notification").removeClass("on");
                            setTimeout(function(){
                                $(".notification").attr("class", "notification clearfix");
                            }, 500);
                        }, 4000);
                    } else{
                        var text = translations["err_server1"];
                        $(".notification").attr("class", "notification clearfix");
                        $(".noti-text").text(text);
                        $(".notification").addClass("error").addClass("on");
                        $("#homeSubscribeRole .modal-close-btn").click();
                        $(".btn_subscribe_on").show();
                        $(".content").removeClass("opacity");
                        $(".subscribe_form input[name='email']").val("");
                        $(".subscribe_form input[name='role']").prop("checked", false);
                        setTimeout(function(){
                            $(".notification").removeClass("on");
                            setTimeout(function(){
                                $(".notification").attr("class", "notification clearfix");
                            }, 500);
                        }, 4000);
                    }
                });
            }
        }, 100);
    });
	
		$(".subscribe-close-btn").click(function(){
			$(".subscribe_on").removeClass("on");
			$(".btn_subscribe_on").show();
			$('.content').toggleClass('opacity');
		});
	
		$(".side-modal-close").click(function(){
			$(".content").removeClass("opacity");
			$(".btn_subscribe_on").show();
			$(".side-modal").css("right", "-100%");
		});
    
    $(".cmain-content-form button[type='submit']").click(function(){
        setTimeout(function(){
            if(!$(".cmain-content-form > form").hasClass("invalid-form")){
                var data = {};
                data.fullname = $(".cmain-content-form input[name='name']").val();
                data.email = $(".cmain-content-form input[name='email']").val();
                data.company = $(".cmain-content-form input[name='company']").val();
                data.subject = $(".cmain-content-form input[name='subject']").val();
                data.message = $(".cmain-content-form textarea").val();
                $.ajax({
                    method: "POST",
                    url: api + "contacts",
                    data: data
                }).then(function(res){
                    console.log(res);
                    var text = translations["success_message"];
                    $(".notification").attr("class", "notification clearfix");
                    $(".noti-text").text(text);
                    $(".notification").addClass("success").addClass("on");
                    $(".cmain-content-form input[name='name']").val("");
                    $(".cmain-content-form input[name='email']").val("");
                    $(".cmain-content-form input[name='company']").val("");
                    $(".cmain-content-form input[name='subject']").val("");
                    $(".cmain-content-form textarea").val("");
                    setTimeout(function(){
                        $(".notification").removeClass("on");
                        setTimeout(function(){
                            $(".notification").attr("class", "notification clearfix");
                        }, 500);
                    }, 4000);
                }, function(err){
                    console.log(err);
                        var text = translations["err_server2"];
                        $(".notification").attr("class", "notification clearfix");
                        $(".noti-text").text(text);
                        $(".notification").addClass("error").addClass("on");
                        $(".cmain-content-form input[name='name']").val("");
                        $(".cmain-content-form input[name='email']").val("");
                        $(".cmain-content-form input[name='company']").val("");
                        $(".cmain-content-form input[name='subject']").val("");
                        $(".cmain-content-form textarea").val("");
                        setTimeout(function(){
                            $(".notification").removeClass("on");
                            setTimeout(function(){
                                $(".notification").attr("class", "notification clearfix");
                            }, 500);
                        }, 4000);
                });
            }
        }, 100);
    });
});



function showModal(id, event){
	if(event && !$(event.target).hasClass("disabled_button")){
		$("body").addClass("modal-open");
		$("html").addClass("modal-open");
		$(".modal-backdrop").show();
		$("#" + id).fadeIn({duration: 400, easing: "swing"});
	} else if(event == undefined || event == null){
		$("body").addClass("modal-open");
		$("html").addClass("modal-open");
		$(".modal-backdrop").show();
		$("#" + id).fadeIn({duration: 400, easing: "swing"});
	}
}

function validateForm(event, el){
    event.preventDefault();
    event.stopPropagation();
    
    var $form = $(el), success = true, emailTrue = true, phoneTrue = true, roleTrue = true;
    
    var temp1 = "<span class='error-msg'>"+translations["err_blank"]+"</span>";
    var temp2 = "<span class='error-msg'>"+translations["err_email1"]+"</span>";
    var temp3 = "<span class='error-msg'>"+translations["err_phone"]+"</span>";
    
    $form.find("input").each(function(i, el){
        if(!$(el).val()){
            success = false;
            return 0;
        }
    });
    
    if($form.find("input[name='role']").length && !$form.find("input[name='role']").is(':checked')){
        success = false;
        roleTrue = false;
    }

    if($form.find("textarea").length && ($form.find("textarea").val().trim() == ""))
        success = false;
 
    if($form.find("input[name='email']").length && $form.find("input[name='email']").val()){
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test($form.find("input[name='email']").val())){
            success = false;
            emailTrue = false;
        }
    }
    
    if($form.find("input[name='phone']").length && $form.find("input[name='phone']").val()){
        var re = /^\d*$/;
        if(!re.test($form.find("input[name='phone']").val())){
            success = false;
            phoneTrue = false;
        }
    }
    console.log(success);
    if(!success){
        $form.find("input").each(function(i, el){
            if(!$(el).val()){
                $(el).addClass("error-input");
                 $(el).parent().find(".error-msg").remove();
                $(el).parent().append(temp1);
            }
            else{
                $(el).removeClass("error-input");
                $(el).parent().find(".error-msg").remove();
            }
        });
        
        if($form.find("textarea").length && ($form.find("textarea").val().trim() == "")){
            $form.find("textarea").addClass("error-input");
            $form.find("textarea").parent().find(".error-msg").remove();
            $form.find("textarea").parent().append(temp1);
        }else{
            $form.find("textarea").removeClass("error-input");
            $form.find("textarea").parent().find(".error-msg").remove();
        }
        
        if(!emailTrue){
            $form.find("input[name='email']").addClass("error-email");
            $form.find("input[name='email']").parent().find(".error-msg").remove();
            $form.find("input[name='email']").parent().append(temp2);
        }
        else if($form.find("input[name='email']").length && $form.find("input[name='email']").val()){
            $form.find("input[name='email']").removeClass("error-email");
            $form.find("input[name='email']").parent().find(".error-msg").remove();
        }
        
        if(!phoneTrue){
            $form.find("input[name='phone']").addClass("error-phone");
            $form.find("input[name='phone']").parent().find(".error-msg").remove();
            $form.find("input[name='phone']").parent().append(temp3);
        }
        else if($form.find("input[name='phone']").length && $form.find("input[name='phone']").val()){
            $form.find("input[name='phone']").removeClass("error-phone");
            $form.find("input[name='phone']").parent().find(".error-msg").remove();
        }
        
        if(!roleTrue){
            $form.find("input[name='role']").addClass("error-role");
            $form.find("input[name='role']").parent().addClass("error-role-box");
        } else {
            $form.find("input[name='role']").removeClass("error-role");
            $form.find("input[name='role']").parent().removeClass("error-role-box");
        }
        
        $form.addClass("invalid-form");
        
    } else {
        $form.find("input").each(function(i, el){
            $(el).removeClass("error-input");
            $(el).removeClass("error-email");
            $(el).removeClass("error-role");
            $(el).parent().removeClass("error-role-box");
        });
        if($form.find("textarea").length)
            $form.find("textarea").removeClass("error-input");
        $(".error-msg").remove();
        
        $form.removeClass("invalid-form");
    }
}

function hideNoti(){
    $(".notification").removeClass("on");
    setTimeout(function(){
        $(".notification").attr("class", "notification clearfix");
    }, 500);
}

function changeLang(l,path){
		if(!path){
			if(l != 'en')
            window.location.href = "/" + l;
			else
					window.location.href = "/";
		} else if(path == 'error'){
            window.location.href = "/" + l + "/" + window.location.href.split("/").slice(-1)[0];
        } else {
			 window.location.href = "/" + l + "/" + path;
		}
    
}

function playRecVideo(id){
    switch(id){
        case '1': 
            videoLink = "https://www.youtube-nocookie.com/embed/dw-tpgNmr98?controls=1&autoplay=1";
            $("#video").attr("src", videoLink);
            break;
        case '2':
            videoLink = "https://www.youtube-nocookie.com/embed/KqkEuN_zeyU?controls=1&autoplay=1";
            $("#video").attr("src", videoLink);
            break;
    }
    
    showModal("videoPromoModal");
}

function validateRecruiter(){
    $(".home_subscribe_form input[name=role][value=recruiter]").attr("checked", "checked");
    $(".home_subscribe_form button[type=submit]").click();
}

function validateTalent(){
    $(".home_subscribe_form input[name=role][value=talent]").attr("checked", "checked");
    $(".home_subscribe_form button[type=submit]").click();
}