var jobapi= "https://test.glorri.az/job-service/", poolapi="https://test.glorri.az/pool-service/", applicationapi="https://test.glorri.az/application-service/", tagapi="https://test.glorri.az/tag-service/", fileapi="https://test.glorri.az/file-service/", talentapi="https://test.glorri.az/talent-service/", companyapi = "https://test.glorri.az/company-service/", questionnaireapi = "https://test.glorri.az/questionnaire-service/", userapi = "https://test.glorri.az/user-service/", monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], timerInterval = null, currentVideoUploadEl = {type: "", el: ""};

var videoDurations = {
    "30 sec": 30,
    "60 sec": 60,
    "2 mins": 120
};

var supportedImageTypes = ['jpeg', 'jpg', 'png', 'bmp', 'gif'];

var supportedExcelTypes = ['csv', 'xlxs', 'xlsm', 'xltx', 'xltm', 'xls', 'xlt', 'xlm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw'];

var supportedWordTypes = ['dot', 'docm', 'dotx', 'dotm', 'docb', 'doc', 'docx'];

var supportedPowerPointTypes = ['pptx', 'ppt', 'pptm','pot', 'pps'];
    
var supportedTextTypes = ['txt', 'rtf', 'odt', 'tex'];
    
var supportedEBookTypes = ['pdf', 'epub', 'mobi', 'djvu', 'rtf'];

var supportedFileTypes = ['jpeg', 'jpg', 'png', 'bmp', 'doc', 'docx', 'txt', 'rtf', 'odt', 'tex', 'csv', 'pdf', 'epub', 'mobi', 'djvu', 'pptx', 'ppt', 'pptm', 'xlxs', 'xlsm', 'xltx', 'xltm', 'dot', 'wbk', 'docm', 'dotx', 'dotm', 'docb', 'xls', 'xlt', 'xlm', 'xlsb', 'xla', 'xlam', 'xll', 'xlw', 'pot', 'pps', 'webm', 'mkv', 'flv', 'vob', 'ogg', 'ogv', 'gif', 'gifv', 'avi', 'TS', 'mov', 'qt', 'wmv', 'amv', 'mp4', 'm4p', 'm4v', 'mpg', 'mp2', 'mpeg', 'mpv', 'mpe', '3gp', 'flv', 'f4v', 'f4p', 'f4a', 'f4b', 'aa', 'aac', 'aax', 'amr', 'au', 'm4a', 'mp3', 'm4b', 'ogg', 'oga', 'mogg', 'opus', 'ra', 'rm', 'voc', 'vox', 'wav', 'wv', 'wma', 'webm'];

function CompanyServices(){
	return {
		addView: function(id){
			return $.ajax({
				method: 'PUT',
				url: jobapi + 'job/'+id+'/view-count-external',
				headers: {
                    'X-Auth-Token': window.localStorage.token
               }
			});
		},
		isCandidateExists: function(companyId, email, userId) {
			return $.ajax({
				method: 'GET',
				url: poolapi + 'candidate-exists-external/'+companyId+'/company/'+userId+'/user/'+email,
				headers: {
                    'X-Auth-Token': window.localStorage.token
               }
			});
		},
		addCandidate: function(candidate){
			return $.ajax({
				method: 'POST',
				url: poolapi + 'candidateExternal',
				contentType: "application/json",
				data: JSON.stringify(candidate), 
				headers: {
                    'X-Auth-Token': window.localStorage.token
               }
			});
		},
		apply: function(app){
			return $.ajax({
				method: 'POST',
				url: applicationapi + 'application/applyExternal/' + app.email,
				contentType: "application/json",
				data: JSON.stringify(app), 
			});
		},
		decline: function(id, reason){
			return $.ajax({
				method: 'PUT',
				url: applicationapi + '/invitation/'+id+'/reject?rejectionNote=' + reason,
				contentType: "application/json",
			});
		},
		isUserExists: function(email){
			return $.ajax({
				method: 'GET',
				url: userapi + 'user/'+email
			});
		},
		register: function(user){
			return $.ajax({
				method: 'POST',
				url: userapi + 'user/register',
				contentType: "application/json",
				data: JSON.stringify(user),
			});
		},
		getActiveJobs: function(id){
			return $.ajax({
				method: 'GET',
				url: jobapi + 'company/'+id+'/jobs-external',
				headers: {
                    'X-Auth-Token': window.localStorage.token
               }
			});
		},
        getJobToken: function(id){
            return $.ajax({
				method: 'POST',
				url: userapi + 'auth/job-token/'+id
			});
        },
		getCompanyInfo: function(id){
			return $.ajax({
				method: 'GET',
				url: companyapi + 'company/'+id+'/profile'
			});
		},
		getJobInfo: function(id){
			return $.ajax({
				method: 'GET',
				url: jobapi + 'job/'+id
			});
		},
		getFileUrl: function(id){
			return $.ajax({
				method: 'GET',
				url: fileapi + 'file/'+id+'/url'
			});
		},
		getBenefit: function(id){
			return $.ajax({
				method: 'GET',
				url: tagapi+"benefit-tag/"+id
			});
		},
		getSkill: function(id){
			return $.ajax({
				method: 'GET',
				url: tagapi + "skill-tag/" + id
			});
		},
		getLanguage: function(id){
			return $.ajax({
				method: 'GET',
				url: tagapi + "language-tag/" + id
			});
		},
		getSoft: function(id){
			return $.ajax({
				method: 'GET',
				url: tagapi + "soft-skill-tag/" + id
			});
		},
		getDepartment: function(id){
			return $.ajax({
				method: 'GET',
				url: tagapi + "department/" + id
			});
		},
		getAllCandidates: function(id){
			return $.ajax({
				method: 'GET',
				url: jobapi + 'job/' + id + '/candidates-count'
			});
		},
		getQuestionnaire: function(id){
			return $.ajax({
				method: 'GET',
				url: questionnaireapi + 'questionnaire/' + id
			});
		},
		getQuestions: function(id){
			return $.ajax({
				method: 'GET',
				url: questionnaireapi + 'questions/' + id
			});
		},
		addSourceTag: function(tag){
			return $.ajax({
				method: 'POST',
				url: tagapi + 'source-tag/addFromExternal/' + tag,
				headers: {
                    'X-Auth-Token': window.localStorage.token
               }
			});
		},
        uploadFile: function(file, percent){
            var formData = new FormData();
            formData.append('file', file);
            formData.append('data', '');
            return $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();

                    // Upload progress
                    xhr.upload.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            //Do something with upload progress
                            //console.log("percent: ", percentComplete);
                            percent(percentComplete);
                        }
                   }, false);

                   return xhr;
                },
				method: 'POST',
				url: fileapi + 'file/public-application/upload?isPrivate=false&name='+file.name,
                data: formData,
                processData: false,
                contentType: false,
                headers: {
                    'X-Auth-Token': window.localStorage.token
               }
			}); 
        },
        uploadResume: function(file, percent){
            var formData = new FormData();
            formData.append('file', file);
            formData.append('data', '');
            return $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();

                    // Upload progress
                    xhr.upload.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            //Do something with upload progress
                            //console.log("percent: ", percentComplete);
                            percent(percentComplete);
                        }
                   }, false);

                   return xhr;
                },
				method: 'POST',
				url: fileapi + 'file/public-application/parseResume?isPrivate=false&name='+file.name,
                data: formData,
                processData: false,
                contentType: false,
                headers: {
                    'X-Auth-Token': window.localStorage.token
               }
			}); 
        },
        uploadVideo: function(file, percent){
            var formData = new FormData();
            formData.append('file', file, file.name);
            formData.append('data', '');
            return $.ajax({
                xhr: function() {
                    var xhr = new window.XMLHttpRequest();

                    // Upload progress
                    xhr.upload.addEventListener("progress", function(evt){
                        if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            //Do something with upload progress
                            //console.log("percent: ", percentComplete);
                            percent(percentComplete);
                        }
                   }, false);

                   return xhr;
                },
				method: 'POST',
				url: fileapi + 'file/public-application/compressVideo?isPrivate=false&name='+file.name,
                data: formData,
                processData: false,
                contentType: false,
                headers: {
                    'X-Auth-Token': window.localStorage.token
               },
                timeout: 0
			}); 
        },
		dateDifference: function(date){
			return new Promise((resolve, reject) => {
				let diffTime = (new Date()).getTime() - (new Date(date)).getTime(), temp = '';

				switch(true){
					case (diffTime/(1000*60*60*24*365) >= 1):
						if(Math.floor(diffTime/(1000*60*60*24*365)) > 1)
							temp = Math.floor(diffTime/(1000*60*60*24*365)) + " years ago";
						else 
							temp = "a year ago";
						break;
					case (diffTime/(1000*60*60*24*30) >= 1):
						if(Math.floor(diffTime/(1000*60*60*24*30)) > 1)
							temp = Math.floor(diffTime/(1000*60*60*24*30)) + " months ago";
						else 
							temp = "a month ago";
						break;
					case (diffTime/(1000*60*60*24*7) >= 1):
						if(Math.floor(diffTime/(1000*60*60*24*7)) > 1)
							temp = Math.floor(diffTime/(1000*60*60*24*7)) + " weeks ago";
						else 
							temp = "a week ago";
						break;
					case (diffTime/(1000*60*60*24) >= 1):
						if(Math.floor(diffTime/(1000*60*60*24)) > 1)
							temp = Math.floor(diffTime/(1000*60*60*24)) + " days ago";
						else 
							temp = "a day ago";
						break;
					case (diffTime/(1000*60*60) >= 1):
						if(Math.floor(diffTime/(1000*60*60)) > 1)
							temp = Math.floor(diffTime/(1000*60*60)) + " hours ago";
						else 
							temp = "an hour ago";
						break;
					case (diffTime/(1000*60) >= 1):
						if(Math.floor(diffTime/(1000*60)) > 1)
							temp = Math.floor(diffTime/(1000*60)) + " minutes ago";
						else 
							temp = "a minute ago";
						break;
					default:
						temp = "just now";
						break;
				}
				
				resolve(temp);
			});
		},
		formatDate: function(date){
			return new Promise((resolve, reject) => {
				var temp = "", datetime = new Date(date);
				temp = monthNames[datetime.getMonth()] + " " + datetime.getDate() + ", " + datetime.getFullYear();
				resolve(temp);
			});
		},
		translateService: function(sufix, key){
			return new Promise((resolve, reject) => {
				let value = TRANSLATIONS.find(t => {return (sufix + key) == t.key});
				if(value !== undefined && value !== null){
					resolve(value);
				} else {
					reject(null);
				}
			});
		},
		shareService: function(type){
			var link = '';
			switch(type){
				case "fb":
					link = "https://www.facebook.com/sharer/sharer.php?u="+window.location.href+"&t=Share on Facebook";
					break;
				case "twitter":
					link = "https://twitter.com/share?url="+window.location.href;
					break;
				case "ln":
					link = "https://www.linkedin.com/shareArticle?mini=true&url="+window.location.href+"&title=Share on Linkedin";
					break;
				case "wp":
					link = "https://wa.me/?text="+window.location.href;
					break;
			}
			let newwindow=window.open(link,'Share on Facebook','height=500,width=500,top=100,left=100,resizable');
            if (window.focus) {newwindow.focus()}
		},
        startTimer: function(time, callback){
            //console.log(time);
            var start = (new Date).getTime() + (time*1000);
            
            timerInterval = setInterval(function() {
                callback(((start - (new Date).getTime()) / 1000));
            }, 1000);
        },
        stopTimer: function(){
            clearInterval(timerInterval);
            timerInterval = null;
        }
	}
}