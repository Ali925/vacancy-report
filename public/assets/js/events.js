/* DOCUMENT INFORMATION
  - Document: Glorri Web Project
  - Version:  1.0
  - Client:   Glorri
  - Author:   Azeroglu Emin
*/
var form, button, help;


$(function () {

    /* [ - Help Object - ] */
    help = {

        Url: function (href) {
            var match = href.match(/^(https?\:)\/\/(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
            return match && {
                href: href,
                protocol: match[1],
                host: match[2],
                hostname: match[3],
                port: match[4],
                pathname: match[5],
                search: match[6],
                hash: match[7]
            }
        },

        YoutubeId: function (url) {
            var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            var match = url.match(regExp);
            return (match && match[7].length == 11) ? match[7] : false;
        },

        VideoUrl: function (link) {
            /* Youtube */
            if (help.Url(link)['hostname'] && help.Url(link)['hostname'] == "www.youtube.com") {
                var response = 'https://www.youtube.com/embed/' + help.YoutubeId(link);
            }
            else {
                var response = link;
            }
            return response;
        },

        getBase64(file, callback) {
           var reader = new FileReader();
           reader.readAsDataURL(file);
           reader.onload = function () {
             callback(reader.result);
           };
        }

    }
    /* [ - Help Object End - ] */

    /* [ - Form Object - ] */
    form = {
        /* |- Variables -| */
        variables: {
            // Select Box Variables
            selectParent: ".form-select-box",
            selectDefault: ".default-value",
            selectContent: ".select-option",
            selectOption: ".option",
            // Line Date Variables
            lineDateParent: ".form-line-date",
            lineDateDefault: ".date",
            // Preview Images
            imgPreviewParent: ".img-content",
            imgPreviewChild: ".img",
            imgPreviewText: ".upload",
            imgPreviewRemoveButton: ".img-remove",
            imgPreviewInputValue: ".image-preview",
            // Add Block
            addBlockParent: ".form-row",
            addBlockChild: ".add-content",
            addBlockCopy: ".row-copy"
        },
        /* |- Variables End -| */

        /* |- Select Box -| */
        selectBox: function (object) {
            var terms = object.attr("data-terms"),
                width = object.data("width"),
                height = object.data("height");

            $("body " + this.variables.selectParent + " " + this.variables.selectContent).hide();
            $("body " + this.variables.selectParent + " " + this.variables.selectDefault).attr("data-terms", "open");

            if (terms == "open") {
                object.attr("data-terms", "close");
                object.parents(this.variables.selectParent).find(this.variables.selectContent).css({
                    width: width + "px",
                    maxHeight: height + "px"
                }).show();
            } else if (terms == "close") {
                object.attr("data-terms", "open");
                object.parents(this.variables.selectParent).find(this.variables.selectContent).hide();
            }

            // Close Selectbox
            allClick({
                parent: this.variables.selectParent,
                parentAllChild: this.variables.selectParent + " *",
                button: this.variables.selectDefault,
                content: this.variables.selectContent,
                params: "data-terms"
            });
        },
        /* |- Select Box End -| */

        /* |- Select Box Selected -| */
        selectBoxSelect: function (object) {
            var value = object.data("value"),
                text = object.text();

            object
                .parents(this.variables.selectParent)
                .find('.select-box-input').val(value);

            object
                .parents(this.variables.selectParent)
                .find(this.variables.selectDefault + " .text")
                .attr("data-value", value);
            object
                .parents(this.variables.selectParent)
                .find(this.variables.selectDefault + " .text span")
                .text(text);
            object
                .parents(this.variables.selectParent)
                .find(this.variables.selectContent)
                .hide();
            object
                .parents(this.variables.selectParent)
                .find(this.variables.selectDefault)
                .attr("data-terms", "open");
        },
        /* |- Select Box Selected End -| */

        /* |- Line Date -| */
        lineDate: function (object) {
            var direction = object.data("direction"),
                month = object
                    .parents(this.variables.lineDateParent)
                    .find(this.variables.lineDateDefault)
                    .attr("data-month");

            if (direction == "left" && month > 1) month--;
            else if (direction == "right" && month < 12) month++;

            object
                .parents(this.variables.lineDateParent)
                .find(this.variables.lineDateDefault)
                .attr("data-month", month);
            object
                .parents(this.variables.lineDateParent)
                .find(this.variables.lineDateDefault + " .month")
                .text(returnMonth(month));
        },
        /* |- Line Date End -| */

        /* |- Images Preview -| */
        imagePreview: function (object, e) {
            var image = object.val(),
                imageExtension = /[.]/.exec(image) ? /[^.]+$/.exec(image) : "",
                acceptExtension = [
                    "png",
                    "PNG",
                    "Png",
                    "jpeg",
                    "jpg",
                    "JPG",
                    "JPEG",
                    "Jpeg",
                    "Jpg"
                ],
                imgType = $.inArray("" + imageExtension + "", acceptExtension),
                reader = "";

            if (imgType < 0) {
                object.val("");
                reader = "";
                object
                    .parents(form.variables.imgPreviewParent)
                    .find(form.variables.imgPreviewText)
                    .show();
                object
                    .parents(form.variables.imgPreviewParent)
                    .find(form.variables.imgPreviewRemoveButton)
                    .hide();
                object
                    .parents(form.variables.imgPreviewParent)
                    .find(form.variables.imgPreviewChild)
                    .css("background-image", "url()");
            } else if (reader != "") {
                reader = "";
                object.val("");
                object
                    .parents(form.variables.imgPreviewParent)
                    .find(form.variables.imgPreviewText)
                    .show();
                object
                    .parents(form.variables.imgPreviewParent)
                    .find(form.variables.imgPreviewRemoveButton)
                    .hide();
                object
                    .parents(form.variables.imgPreviewParent)
                    .find(form.variables.imgPreviewChild)
                    .css("background-image", "url()");
            } else {
                var input = $(e.currentTarget),
                    file = input[0].files[0];

                reader = new FileReader();

                reader.onload = function (a) {
                    image_base64 = a.target.result;

                    if (image_base64.length > 2000) {
                        object
                            .parents(form.variables.imgPreviewParent)
                            .find(form.variables.imgPreviewText)
                            .hide();
                        object
                            .parents(form.variables.imgPreviewParent)
                            .find(form.variables.imgPreviewRemoveButton)
                            .show();
                        object
                            .parents(form.variables.imgPreviewParent)
                            .find(form.variables.imgPreviewChild)
                            .css("background-image", "url(" + image_base64 + ")");
                    } else {
                        reader = "";
                        object.val("");
                        object
                            .parents(form.variables.imgPreviewParent)
                            .find(form.variables.imgPreviewText)
                            .show();
                        object
                            .parents(form.variables.imgPreviewParent)
                            .find(form.variables.imgPreviewRemoveButton)
                            .hide();
                        object
                            .parents(form.variables.imgPreviewParent)
                            .find(form.variables.imgPreviewChild)
                            .css("background-image", "url()");
                    }
                };

                reader.readAsDataURL(file);
            }
        },
        /* |- Images Preview End -| */

        /* |- Images Preview Remove -| */
        imagePreviewRemove: function (object) {
            object
                .parents(form.variables.imgPreviewParent)
                .find(form.variables.imgPreviewInputValue)
                .val("");
            object
                .parents(form.variables.imgPreviewParent)
                .find(form.variables.imgPreviewText)
                .show();
            object
                .parents(form.variables.imgPreviewParent)
                .find(form.variables.imgPreviewRemoveButton)
                .hide();
            object
                .parents(form.variables.imgPreviewParent)
                .find(form.variables.imgPreviewChild)
                .css("background-image", "url()");
        },
        /* |- Images Preview Remove End -| */

        /* |- Add Block -| */
        addBlock: function (object) {
            var block = object.parents(form.variables.addBlockParent)
                    .find(form.variables.addBlockChild + " " + form.variables.addBlockCopy + ":first")
                    .clone(),
                blockCount = object
                    .parents(form.variables.addBlockParent)
                    .find(
                        form.variables.addBlockChild + " " + form.variables.addBlockCopy
                    ).length;

            if (blockCount < 5) {


                block.find("input").val("");
                block.find("input").prop('checked', false);
                block.append('<span class="remove-block icon-cross"></span>');
                $(form.variables.addBlockParent)
                    .find(form.variables.addBlockChild)
                    .append(block);
            }
        },
        /* |- Add Block End -| */

        /* |- Add Block Remove -| */
        addBlockRemove: function (object) {
            object.parents(form.variables.addBlockCopy).remove();
        },
        /* |- Add Block Remove End -| */

        /* |- Video Upload Input -| */
        videoUploadInput: function (object) {
            var value = object.val();
            $('.js-video-upload-iframe').slideDown(100).find('iframe').attr('src', help.VideoUrl(value));
            object.val(help.VideoUrl(value));

            $('.js-video-upload-iframe iframe').ready(function (e) {
                $(".js-video-add-button").fadeIn(100);
            });
        },
        /* |- Video Upload Input End -| */

        /* |- Video Upload Add -| */
        videoUploadAdd: function (object) {
            var link = object.parent().find('.js-video-upload-input').val();
            $('.js-video-preview .upload').hide();
            $('.js-video-preview .preview').removeClass('display-none');
            $('.js-video-preview .preview .input').val(link);
            $('.js-video-preview .preview .view').attr('src', link);
            button.ModalBlockClose(object, 'button');
            object.parent().find('.js-video-upload-input').val('');
            $('.js-video-upload-iframe').hide().attr('src', '');
        },
        /* |- Video Upload Add End -| */

        /* |- Video Upload Cancel -| */
        videoUploadCancel: function (object) {
            object.parents('.js-video-preview').find('.preview').addClass('display-none');
            object.parents('.js-video-preview').find('.preview .input').val('');
            object.parents('.js-video-preview').find('.preview .view').attr('src', '');
            object.parents('.js-video-preview').find('.upload').show();
        },
        /* |- Video Upload Cancel End -| */

        /* |- Map Upload Input -| */
        mapUploadInput: function (object) {
            var value = object.html(),
                split1 = value.split('scr="');

            console.log(split1[0]);

        },
        /* |- Map Upload Input End -| */

        /* |- Form Collapse -| */
        collapse: function (object) {
            var content = object.data('content'),
                terms = object.attr('data-terms'),
                parent = '.form-collapse';

            if (terms == "open") {
                object.attr('data-terms', 'close');
                object.parents(parent).find("div[id=" + content + "]").stop().slideUp(200);
                object.find('.icon').removeClass('icon-102');
                object.find('.icon').addClass('icon-101');
            }
            else if (terms == "close") {
                object.attr('data-terms', 'open');
                object.parents(parent).find("div[id=" + content + "]").stop().slideDown(200);
                object.find('.icon').removeClass('icon-101');
                object.find('.icon').addClass('icon-102');
            }

        },
        /* |- Form Collapse End -| */

        /* |- Form Step -| */
        step: function (object) {
            var index = object.index(),
                stepContent = '.form-step-content',
                stepLink = '.js-form-step';

            $('body ' + stepLink).find('.step-icon').removeClass('icon-102');
            $('body ' + stepLink).find('.step-icon').addClass('icon-101');
            object.find('.step-icon').removeClass('icon-101');
            object.find('.step-icon').addClass('icon-102');
            $('body ' + stepLink).removeClass('active');
            object.addClass('active');

            $('body ' + stepContent).hide();
            $('body ' + stepContent + ':eq(' + index + ')').fadeIn(200);

        },
        /* |- Form Step End -| */

        /* |- Form Add Modal Element -| */
        addModalElement: function (object) {
            var parent = object.data('parent');
            ;
            var element = object.data('element');
            var value = object.parents(element).find('.js-add-modal-element').val();
            value = $.trim(value);

            if (value != "") {
                $(element)
                    .find('#add-modal-element')
                    .find('.label').text(value);
                var block = object.parents(element)
                    .find('#add-modal-element')
                    .clone()
                    .removeAttr('id')
                    .removeClass('display-none')
                    .addClass('mt-25');
                $(parent).append(block[0].outerHTML);
                object.parents(element).find('.js-add-modal-element').val('');
                button.ModalBlockClose(object, 'button');
            }


        },
        /* |- Form Add Modal Element End -| */

        /* |- Form Tags -| */
        tags: function (object, e) {
            if ( e.which == 13 ) {
                var terms   =   object.data('terms') ? object.data('terms') : '';
                var value   =   terms == 'input' ? object.val() : object.text();
                var name    =   object.data('name');
                var parent  =   '.form-tags';
                var tagContent    =  '.tag-content';

                if ( terms == 'input' ) {
                    var tags  = '<div class="chk">' +
                                '<span class="txt">'+value+'</span>' +
                                '<i class="icon icon-20" onclick="$(this).parent().remove();"></i>' +
                                '<input type="hidden" name="'+name+'" value="'+value+'">' +
                                '</div>';
                }
                else {
                    var tags  = '<div class="tag">' +
                                '<span>'+value+'</span>' +
                                '<input type="hidden" name="'+name+'" value="'+value+'">' +
                                '<span class="remove icon-20" onclick="$(this).parent().remove()"></span>' +
                                '</div>';
                }
                object.parents(parent).find(tagContent).append(tags);
                if ( terms == 'input' ) {
                    object.val('');
                }
                else {
                    object.text('');
                }
            }
        },
        /* |- Form Tags End -| */


    };
    /* [ - Form Object End - ] */

    /* # [ - Button Object - ] # */
    button = {

        /* [ Images ] */
        imagesView: function (object) {
            var file    =   object[0].files[0];
            var name    =   file.name;
            var type    =   file.type;
            var nameBlock   =   object.attr('data-name-block');
            var viewBlock   =   object.attr('data-view-block');
            var closeItem   =   object.attr('data-close-item') ? object.attr('data-close-item') : 0;
            help.getBase64(file, function (base64) {
                if ( type == 'image/jpeg' || type == 'image/png' ) {
                    object.parents('.js-parents').find(nameBlock).text(name);
                    object.parents('.js-parents').find(viewBlock).attr('src', base64);
                    object.parents('.js-parents').find('.js-images-remove').show();
                    if ( closeItem.length ) {
                        object.parents('.js-parents').find(closeItem).hide();
                    }
                }
            });
        },
        /* [ Images End ] */

        /* [ Images Remove ] */
        imagesRemove: function (object) {
            var defaultImg  =   object.attr('data-image');
            var viewBlock   =   object.attr('data-view-block');
            var defaultName = object.attr('data-name');
            var nameBlock   =   object.attr('data-name-block');
            var closeItem   =   object.attr('data-close-item');
            object.parents('.js-parents').find(viewBlock).attr('src',defaultImg);
            object.parents('.js-parents').find(nameBlock).text(defaultName);
            object.parents('.js-parents').find('.js-images-view').val('');
            object.hide();
            if ( closeItem.length ) {
                 object.parents('.js-parents').find(closeItem).show();
            }
        },
        /* [ Images Remove End ] */

        /* [ Dropdown Menu ] */
        dropdownMenu: function (object) {
            var menu     =   '.js-dropdown-menu';
            var submenu     =   '.js-sub-menu';

            if ( object.find(submenu).length ) {
                $('body '+menu).removeClass('active');
                $('body '+menu+' '+submenu).slideUp(100);
                object.addClass('active');
                object.find(submenu).slideDown(100);
            }



        },
        /* [ Dropdown Menu ] */


        /* [ Follow Method ] */
        follow: function (object) {
            var innerText = object.text(),
                text = object.attr("data-text"),
                terms = object.attr("data-terms");

            if (terms == "follow") {
                object.attr("data-terms", "unfollow");
                object.removeClass("button-primary").addClass("button-danger");
            } else if (terms == "unfollow") {
                object.attr("data-terms", "follow");
                object.removeClass("button-danger").addClass("button-primary");
            }

            object.text(text);
            object.attr("data-text", innerText);
        },
        /* [ Follow Method End ] */

        /* [ Block Open Method ] */
        blockOpen: function (object) {
            var parent = ".show-block",
                child = ".show-content",
                shadow = ".show-content-shadow";
                icon = ".icon";
                terms = object.attr("data-terms"),
                innerText = object.find("span").text(),
                text = object.attr("data-text");

            if (terms == "open") {
                object.attr("data-terms", "close");
                object.parents(parent).find(child).addClass("height-auto");
                object.parents(parent).find(shadow).addClass("display-none");
                object.find(icon).removeClass("icon-102");
                object.find(icon).addClass("icon-104");
            } else if (terms == "close") {
                object.parents(parent).find(child).removeClass("height-auto");
                object.parents(parent).find(shadow).removeClass("display-none");
                object.find(icon).removeClass("icon-104");
                object.find(icon).addClass("icon-102");
                object.attr("data-terms", "open");
            }

            object.find("span").text(text);
            object.attr("data-text", innerText);
        },
        /* [ Block Open Method End ] */


        /* [ Modal Block Method ] */
        ModalBlock: function (object) {
            var type = object.data('type');
            var path = object.data('path');
            var closed = object.data('closed') ? object.data('closed') : 'yes';
            $('body').addClass('overflow-hidden');
            if (closed == 'yes') {
                $(".box-modal .modal-block").removeClass('open');
                $(".box-modal").fadeOut(300);
            }
            var modal = '.' + object.data('class');
            $(modal).fadeIn(100, function (){
                $(".box-modal .modal-block").addClass('open');
            });
            $(".box-modal .modal-block").addClass('open');
            if (type == 'load' && path) {
                $('#modal-load-box').load(path, function () {
                    $(".wrapper").scrollbar({
                        disableBodyScroll: false
                    });
                });
            }
        },
        ModalBlockClose: function (object, terms) {
            if (terms == 'all') {
                if (!$(object.target).is('.modal-block') && !$(object.target).is('.modal-block *')) {
                    $('body').removeClass('overflow-hidden');
                    $(".box-modal .modal-block").removeClass('open');
                    $(".box-modal").fadeOut(300);
                }
            }
            else if (terms == 'button') {
                $('body').removeClass('overflow-hidden');
                $(".box-modal .modal-block").removeClass('open');
                $(".box-modal").fadeOut(300);
            }

        },
        /* [ Modal Block Method End ] */

        /* [ Dropdown Method ] */
        dropdown: function (object) {
            var parent = '.js-dropdown-parent',
                child = '.dropdown-content',
                button = '.js-dropdown',
                terms = object.attr('data-terms');

            $('body ' + parent + ' ' + child).hide();
            $('body ' + parent + ' ' + button).removeClass('active');


            if (terms == "open") {
                object.addClass('active');
                object.attr('data-terms', "close");
                object.parents(parent).find(child).show();
            }
            else if (terms == "close") {
                object.removeClass('active');
                object.attr('data-terms', "open");
                object.parents(parent).find(child).hide();
            }

            // Close Selectbox
            allClick({
                parent: parent,
                parentAllChild: parent + " *",
                button: button,
                content: child,
                params: "data-terms",
                iconActive: 'yes'
            });

        },
        /* [ Dropdown Method End ] */

        /* [ Tab System ] */
        tabSystem: function (object) {
            var parent = '.' + object.data('parent');
            var box = '.js-tab-system-box';
            var tab = '.js-tab-system';
            var index = object.index();
            var type = object.data('type') ? object.data('type') : '';

            object.parents(parent).find(box).removeClass('active');
            object.parents(parent).find(box + ':eq(' + index + ')').addClass('active');
            object.parents(parent).find(tab).removeClass('active');
            object.addClass('active');

            if (type == 'pipeline-checkbox' || type == 'invitations-checkbox' || type == 'filters-checkbox') {
                $('#bulk-button').attr({'data-type': type, 'data-terms': 'open'});
                $('#bulk-button').find('.icon').addClass('display-none');
                $('.default-checkbox').addClass('display-none');
            }

            if (type == 'invitations-checkbox') {
                $('.tab-pipeline').addClass('display-none');
                $('.invite-button').addClass('display-none');
                $('.invite-job-btn').removeClass('display-none');
            }
            else {
                $('.tab-pipeline').removeClass('display-none');
                $('.invite-button').removeClass('display-none');
                $('.invite-job-btn').addClass('display-none');
            }

        },
        /* [ Tab System End ] */

        /* [ Tab System sub ] */
        subTabSystem: function (object, parent, content, link) {
            var index = object.index();
            object.parents(parent).find(link).removeClass('active');
            object.addClass('active');
            object.parents(parent).find(content).removeClass('active');
            object.parents(parent).find(content + ':eq(' + index + ')').addClass('active');
        },
        /* [ Tab System sub End ] */

        /* [ Bulk System ] */
        bulk: function (object) {
            var checkbox = object.attr('data-type');
            var terms = object.attr('data-terms');

            if (terms == 'open') {
                $('#bulk-head').addClass('active');
                $('#tab-head').removeClass('active');
                object.find('.icon').removeClass('display-none');
                object.attr('data-terms', 'close');
                $('.' + checkbox).removeClass('display-none');
            }
            else if (terms == 'close') {
                $('#tab-head').addClass('active');
                $('#bulk-head').removeClass('active');
                object.find('.icon').addClass('display-none');
                object.attr('data-terms', 'open');
                $('.' + checkbox).addClass('display-none');
            }

        },
        /* [ Bulk System End ] */

        /* [ Sub Menu ] */
        subMenu: function (object) {
            var parent = '.js-sub-parent';
            var child = '.js-sub-child';
            var link = '.js-sub-menu';
            var event = object.parents(parent).attr('data-event') ? object.parents(parent).attr('data-event') : 'yes';
            var terms = object.attr('data-terms');

            if (event == 'yes') {
                $(parent).find(child).slideUp(100);
                $(parent).find(link).attr('data-terms', 'open');
            }

            if (terms == 'open') {
                object.attr('data-terms', 'close');
                object.find(child).slideDown(100);
                object.find('.n-menu .icon').removeClass('icon-102');
                object.find('.n-menu .icon').addClass('icon-104');
            }
            else if (terms == 'close') {
                object.attr('data-terms', 'open');
                object.find(child).slideUp(100);
                object.find('.n-menu .icon').removeClass('icon-104');
                object.find('.n-menu .icon').addClass('icon-103');
            }

        },
        /* [ Sub Menu End ] */

        /* [ Remove Data ] */
        removeData: function (object) {
            var parent = '.' + object.data('parent');
            object.parents(parent)
                .animate({
                    marginTop: '-10px',
                })
                .fadeOut(100, function () {
                    $(this).remove();
                });
        },
        /* [ Remove Data End ] */

        /* [ All Checked ] */
        allChecked: function (object) {
            var parent = object.data('parent');
            var checkbox = '.js-checkbox-all';
            if (object[0].checked == true) {
                object.parents('.' + parent)
                    .find(checkbox)
                    .prop('checked', true);
            }
            else {
                object.parents('.' + parent)
                    .find(checkbox)
                    .prop('checked', false);
            }
        },
        /* [ All Checked End ] */

        /* [ Add Element ] */
        addElement: function (object) {
            var parent = '.' + object.data('parent');
            var child = '.' + object.data('child');
            var inputName = object.data('input-name');
            var clone = object.parents(parent).find('#add-copy').clone().removeClass('display-none').removeAttr('id').attr('value', '');
            clone = clone.addClass('mt-5').attr('name', inputName);
            object.parents(parent).find(child).append(clone[0].outerHTML);
        },
        /* [ Add Element End ] */

        /* [ View ] */
        view: function (object) {
            var openBlock = object.data('open');
            var closedBlock = object.data('closed');
            var buttonClass = object.data('button-class');
            var terms = object.data('terms');

            if (terms == 'open') {
                $('.' + buttonClass).hide();
                $(openBlock)
                    .addClass('display-none');
                $(closedBlock)
                    .removeClass('display-none');
            }
            else if (terms == 'close') {
                $('.' + buttonClass).show();
                $(openBlock)
                    .removeClass('display-none');
                $(closedBlock)
                    .addClass('display-none');
            }


        },
        /* [ View End ] */

        /* [ Element Add ] */
        elmentAdd: function (object) {
            var klass = $(object.data('clone'));
            var content = $(object.data('content'));
            var clone = klass.clone().removeAttr('id');
            var finalClone = clone.clone();
            finalClone.find('input').attr('value', '');
            content.append(finalClone);
        },
        /* [ Element Add End ] */

        /* [ Tab Systems ] */
        tabSystems: function (object) {
            var tabContainer = '.js-tab-container';
            var id = object.data('id');
            var tablink = '.js-tap-systems';
            object.parents(tabContainer).find(tablink).removeClass('active');
            object.addClass('active');
            object.parents(tabContainer).find('.js-tab-system-content').hide();
            object.parents(tabContainer).find('.js-tab-system-content' + id).show();
        },
        /* [ Tab Systems End ] */

        /* [ App Dropdown ] */
        appDropdown: function (object) {
            var parent = '.app-dropdown-parent';
            var block = '.app-dropdown-block';
            var event = '.app-dropdown';
            var id = object.data('id');
            var terms = object.attr('data-terms');

            // object.parents(parent)
            //     .find(block)
            //     .slideUp(100);
            // object.parents(parent)
            //     .find(event)
            //     .attr('data-terms','open');

            if (terms == 'open') {
                object.attr('data-terms', 'close');
                object.parents(parent)
                    .find(id)
                    .slideDown(100);
                object.find('.icon').removeClass('icon-102').addClass('icon-104');
            }
            else if (terms == 'close') {
                object.attr('data-terms', 'open');
                object.parents(parent)
                    .find(id)
                    .slideUp(100);
                object.find('.icon').removeClass('icon-104').addClass('icon-102');
            }
        },
        /* [ App Dropdown End ] */

        /* [ App Photo View ] */
        appPhotoView: function (object, e) {
            var parent = '.app-photo-parent';
            var viewBlock = object.data('view-block');
            var dfltImg =   object.data('default-img');
            var removeIcon = '.remove-icon';

            var image = object.val(),
                imageExtension = /[.]/.exec(image) ? /[^.]+$/.exec(image) : "",
                acceptExtension = [
                    "png",
                    "PNG",
                    "Png",
                    "jpeg",
                    "jpg",
                    "JPG",
                    "JPEG",
                    "Jpeg",
                    "Jpg"
                ],
                imgType = $.inArray("" + imageExtension + "", acceptExtension),
                reader = "";

            if (imgType < 0 || reader != "") {
                object.val("");
                reader = "";
                object
                    .parents(parent)
                    .find(removeIcon)
                    .hide();
                object
                    .parents(parent)
                    .find(viewBlock)
                    .attr('src', dfltImg);
            }
            else {
                var input = $(e.currentTarget),
                    file = input[0].files[0];

                reader = new FileReader();

                reader.onload = function (a) {
                    image_base64 = a.target.result;

                    if (image_base64.length > 2000) {
                        object
                            .parents(parent)
                            .find(removeIcon)
                            .show();
                        object
                            .parents(parent)
                            .find(viewBlock)
                            .attr("src", image_base64);
                    } else {
                        reader = "";
                        object.val("");
                        object
                            .parents(parent)
                            .find(removeIcon)
                            .hide();
                        object
                            .parents(parent)
                            .find(viewBlock)
                            .attr('src', dfltImg);
                    }
                };

                reader.readAsDataURL(file);
            }
        },
        /* [ App Photo View End ] */

        /* [ App Photo View Remove ] */
        appPhotoViewRemove: function (object) {
            var parent = '.app-photo-parent';
            var viewBlock = '.view';
            var inputVal  = '.app-photo-view';
            var dfltImg   = object.parents(parent).find(inputVal).data('default-img');
            object.parents(parent).find(viewBlock).attr('src',dfltImg);
            object.parents(parent).find(inputVal).val('');
            object.hide();
        },
        /* [ App Photo View Remove End ] */

        /* [ Add Buttons ] */
        addButtons: function (object) {
            var content     =   object.data('copy-content');
            var block     =   object.data('copy-block');
            var cloneBlock  = $('#'+block).clone().removeAttr('id');
            var text    =   object.data('text');
            var number  =   $('.'+content).find(block).length;
            number = parseInt(number) + 1;
            object.attr('data-copy-number',number);
            cloneBlock.find('input').val("");
            cloneBlock.find('.remove').show();
            $('.'+content).append(cloneBlock);
        },
        /* [ Add Buttons End ] */

        /* [ Dropdown Block ] */
        dropdownBlock: function (object) {
            var terms   =   object.attr('data-terms');
            var block   =   object.data('id');
            var parent = '.dropdown-parent';

            $('body '+parent).hide();

            if ( terms == 'open' ) {
                object.attr('data-terms','close');
                $(block).show();
            }
            else if ( terms == 'close' ) {
                object.attr('data-terms','open');
                $(block).hide();
            }
        },
        /* [ Dropdown Block End ] */

        /* [ Rating ] */
        rating: function (object, e) {
            var parent  =   '.js-rating';
            var child   =   '.star';
            var value   =   parseInt(object.data('value'));
            object.parents(parent).find(child).removeClass('active');
            for ( var i = 0; i < value; i++ ) {
                object.parents(parent).find(child+':eq('+i+')').addClass('active')
            }
        },
        /* [ Rating End ] */

        /* [ Picture Add ] */
        pictureAdd: function (object, e) {
            if ( e.keyCode == 13 ) {
                var text    =   $.trim(object.val());
                var content     =   $('._picture_content');
                var clone   =   $('#_picture_clone').clone().removeAttr('id');
                var img =   clone.find('._images').attr('src');

                if (text != "" ) {
                    clone.find('input').val('');
                    clone.find('._images').attr('src', '_assets/img/defaultImg.svg');
                    clone.find('.remove').show();
                    content.append(clone);
                    clone.find('input[type=text]').focus();
                }
            }
        },
        /* [ Picture Add End ] */

        /* [ Question Type ] */
        questionType: function (object) {
            var id  =   object.val();
            $('body .form-attribute').hide();
            $('body .form-attribute .__rows').hide();
            $('body #'+id).parents('.form-attribute').show();
            $('body .form-attribute #'+id).show();
        },
        /* [ Question Type End ] */

        /* [ Question Edit ] */
        questionEdit: function (object) {
            var block   =   object.parents('.q-list').find('.block');
            var form    =   object.parents('.q-list').find('.form');
            var terms   =   object.attr('data-terms');
            var type    =   object.attr('data-type') ? object.attr('data-type') : '';
            var addButton   =   $('.questions-list-page .q-content .q-list .__question_form');

            if ( addButton.length ) {
                addButton.show();
            }

            $('.questions-list-page .q-content .q-list .form').hide();
            if (block.length) {
                $('.questions-list-page .q-content .q-list .block').show();
            }
            if ( terms == 'open' ) {
                if (type == 'add') {
                    object.hide();
                }
                if (block.length) {
                    block.hide();
                }
                form.show();
            }
            else if (terms == 'close') {
                form.hide();
                if (block.length) {
                    block.show();
                }
                if (type == 'add') {
                    object.show();
                }
            }
        },
        /* [ Question Edit End ] */


    };
    /* # [ - Button Object End - ] # */


});
