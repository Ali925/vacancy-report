var application = {}, user = {}, questions = [], companyService = CompanyServices(), _SOURCE = '', _INVITATION = '', _REFFERAL = '', _ORIGIN = '', _EMAIL = '';
application.name = {first: '', last: ''};
application.questions = [];
user.name = {first: '', last: ''};
$(document).ready(function(){

	if($(window).width() <= 450){
		$(".desktop-view").remove();
	} else {
		$(".mobile-view").remove();
	}

	_SOURCE = getParameterByName("source");
	_INVITATION = getParameterByName("invitation");
	_REFFERAL = getParameterByName("ref");
	_ORIGIN = getParameterByName("origin");
	_EMAIL = getParameterByName("email");

	if(_SOURCE)
		application.origin = _SOURCE;
	if(_INVITATION)
		application.invitationId = _INVITATION;
	if(_SOURCES.title !== '___empty___'){
		_SOURCE = 'Sourced';
		application.origin = _SOURCE;
		companyService.addSourceTag(_SOURCES.title.charAt(0).toUpperCase() + _SOURCES.title.substring(1))
			.then(function(res){
				console.log(res);
		        application.sourceTags = [];
		        application.sourceTags.push(res.id);
			}, function(err){
				console.log(err);
			});
	}
	if(_REFFERAL){
		_SOURCE = 'Reffered';
		application.origin = _SOURCE;
		application.refferal = _REFFERAL;
	}

	if(_ORIGIN == 'Email' && _EMAIL && _EMAIL.trim().length){
		_SOURCE = 'Email';
		application.origin = _ORIGIN;
		application.name.first = getParameterByName("firstname");
		application.name.last = getParameterByName("lastname");
		application.email = _EMAIL;
		user.name.first = getParameterByName("firstname");
		user.name.last = getParameterByName("lastname");
		user.email = _EMAIL;

		$("input[name='firstName']").val(getParameterByName("firstname"));
		$("input[name='lastName']").val(getParameterByName("lastname"));
		$("input[name='email']").val(_EMAIL);

		setTimeout(function(){
			validateForm();
		}, 1000);
		

	} else if(_ORIGIN == 'Email'){
		_SOURCE = 'Email';
		application.origin = _ORIGIN;
	}

	$(window).scroll(function(){
		console.log("scroll top: ", document.body.scrollTop, document.documentElement.scrollTop, document.documentElement.scrollTop - document.documentElement.offsetHeight);
		if(document.documentElement.scrollTop > 180){
			if(document.documentElement.scrollTop - document.documentElement.offsetHeight < -620){
				var top = (document.documentElement.scrollTop - 180) + 'px';
				$(".body_block.right").css("top", top);
			} 
		} else {
			$(".body_block.right").css("top", '0px');
		}
	});

    companyService.getJobToken(JOB_INFO.id)
    .then(function(res){
		//console.log(res);
        window.localStorage.token = res;
	}, function(err){
		console.log(err);
	});
	
	companyService.getFileUrl(JOB_INFO.coverImage)
	.then(function(res){
		//console.log(res);
		$(".desktop-view .header_cover").css("background-image", "url(" + res + ")");
	}, function(err){
		console.log(err);
	});

	companyService.getFileUrl(COMPANY_INFO.logo)
	.then(function(res){
		//console.log(res);
		$(".company_logo").append("<img src='"+res+"' />");
	}, function(err){
		console.log(err);
	});
	
	companyService.translateService("job-types.", JOB_INFO.type)
	.then(function(res){
		//console.log("job type: ", res);
		$("#job_type > span").text(res.value);
	}, function(err){
		$("#job_type").hide();
		console.log(err);
	});
	
	companyService.dateDifference(JOB_INFO.postedDate)
	.then(function(res){
		//console.log(res);
		$("#job_time > span").text("Posted " + res);
	}, function(err){
		console.log(err);
	});

	companyService.getQuestionnaire(APP_REQ.questionnaire)
	.then(function(res){
		console.log('questionnaire: ', res);
		if(res.passScore)
			application.screeningPassScore = res.passScore;
	}, function(err){
		console.log(err);
	});
	
	companyService.getQuestions(APP_REQ.questionnaire)
	.then(function(res){
		console.log('questions: ', res);
		questions = res.sort(function(a, b){
			if((a.questionType == 'screen' && (b.questionType == 'scored' || b.questionType == 'unscored')) || (a.questionType == 'scored' && b.questionType == 'unscored'))
				return -1;
			else if((a.questionType == 'unscored' && (b.questionType == 'scored' || b.questionType == 'screen')) || (a.questionType == 'scored' && b.questionType == 'screen'))
				return 1;
			else{
				if(a.ratings > b.ratings)
					return 1;
				else if(b.ratings > a.ratings)
					return -1;
				else 
					return 0;
			}
		});	
		buildQuestionsTemplate(questions);
	}, function(err){
		console.log(err);
	});
	
	$(".share_buttons > div").click(function(){
		var type = $(this).attr('class').substr(0, $(this).attr('class').length-4);
		companyService.shareService(type);
	});
    
    $(".email_btn").click(function(){
        var url = window.location.href;
        window.open('mailto:?subject='+JOB_INFO.title+'&body='+window.location.href, "_blank", "width=800,height=800");
    });
	
	$(".details_apply_social_btns > .fb-btn").click(function(){
		FB.login(function(response) {
  		// handle the response
			//console.log(response);
			if(response.authResponse)
              {
                  FB.api('/me?fields=email,first_name,last_name,picture.width(800).height(800)', function(responseFromFB){
										//console.log(responseFromFB);
										$("input[name='firstName']").val(responseFromFB.first_name);
										$("input[name='lastName']").val(responseFromFB.last_name);
										$("input[name='email']").val(responseFromFB.email);
										//$("input[name='profile_photo']").val(responseFromFB.picture.data.url);
										$(".profile_photo_preview").css("background-image", "url(" + responseFromFB.picture.data.url + ")");
										$("input[name='profile_photo']").parent().parent().next().removeClass("empty");
                  });
              }
              else
              {
                  console.log('The login failed because they were already logged in');
              }
			}, {scope: 'public_profile,email'});
	});
	
	$(".details_apply_social_btns > .ln-btn").click(function(){
        
		let client = new jso.JSO({
			providerID: "linkedin",
			response_type: "code",
			client_id: "779lm5lixvz91l",
			client_secret: "1ZowcxTWQNbU8VCQ",
			redirect_uri: "https://glorri.az/company/login/linkedin", // The URL where you is redirected back, and where you perform run the callback() function.
			authorization: "https://www.linkedin.com/oauth/v2/authorization",
            scopes: {request: ["r_emailaddress", "r_liteprofile", "w_member_social"]}
		});
		
		client.callback();
		
		let opts = {
			redirect_uri: "https://glorri.az/company/login/linkedin"
		};
		
		client.setLoader(jso.Popup);
		
		client.getToken(opts)
			.then((token) => {
				//console.log("I got the token: ", LN_TOKEN, LN_EMAIL);
                $("input[name='firstName']").val(LN_TOKEN.firstName.localized[Object.keys(LN_TOKEN.firstName.localized)[0]]);
				$("input[name='lastName']").val(LN_TOKEN.lastName.localized[Object.keys(LN_TOKEN.lastName.localized)[0]]);
				$("input[name='email']").val(LN_EMAIL.elements[0]["handle~"].emailAddress);
				//$("input[name='profile_photo']").val(responseFromFB.picture.data.url);
				$(".profile_photo_preview").css("background-image", "url(" + LN_TOKEN.profilePicture["displayImage~"].elements[LN_TOKEN.profilePicture["displayImage~"].elements.length-1].identifiers[0].identifier + ")");
				$("input[name='profile_photo']").parent().parent().next().removeClass("empty");
			})
            .catch((err) => {
				console.error("Error from passive loader", err)
        })
    });
	
});

function submitApp(){
	if(validateForm()){
		$(".details_apply_button").addClass("disabled_button");
		application.jobId = JOB_INFO.id;
		application.jobCreatedBy = JOB_INFO.createdBy;
		application.companyId = COMPANY_INFO.id;
		user.name = application.name;
		user.email = application.email;
		user.language = "en";
		user.type = 2;
		user.password = Math.random().toString(36).slice(-6);
		user.status = 5;
		user.timezoneOffset = (new Date()).getTimezoneOffset();
    	user.timezone = moment.tz.guess();
		candidate = {};
		candidate.name = application.name;
		candidate.companyId = COMPANY_INFO.id;
		candidate.emails = [application.email];
		if(application.resume){
			candidate.resume = application.resume;
			candidate.resumeText = application.resumeText;
		}
		if(user.avatar)
			candidate.photo = user.avatar;

		companyService.isUserExists(user.email)
			.then(function(userInfo){
				application.applierId = userInfo.id;
						candidate.userId = userInfo.id;
						companyService.isCandidateExists(COMPANY_INFO.id, application.email, userInfo.id).then(function(response){
									console.log(response);
									application.candidateId = response.id;
									companyService.apply(application)
										.then(function(res){
											console.log(res);
											// $(".details_apply_button").removeClass("disabled_button");
											// $(".body_block.right").hide();
											// $(".body_block.left").hide();
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=true";
											else
												window.location.href = window.location.href + "?applied=true";
										}, function(err){
											console.log(err);
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=false";
											else
												window.location.href = window.location.href + "?applied=false";
										});
						}, function(err){
							companyService.addCandidate(candidate)
								.then(function(response){
									console.log(response);
									application.candidateId = response.id;
									companyService.apply(application)
										.then(function(res){
											console.log(res);
											// $(".details_apply_button").removeClass("disabled_button");
											// $(".body_block.right").hide();
											// $(".body_block.left").hide();
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=true";
											else
												window.location.href = window.location.href + "?applied=true";
										}, function(err){
											console.log(err);
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=false";
											else
												window.location.href = window.location.href + "?applied=false";
										});
								}, function(err){
									console.log(err);
								});
						});
			}, function(err){
				console.log(err);
				companyService.register(user)
					.then(function(res){
						application.applierId = res.id;
						//candidate.userId = res.id;

						companyService.isCandidateExists(COMPANY_INFO.id, application.email, null).then(function(response){
									console.log(response);
									application.candidateId = response.id;
									companyService.apply(application)
										.then(function(res){
											console.log(res);
											// $(".details_apply_button").removeClass("disabled_button");
											// $(".body_block.right").hide();
											// $(".body_block.left").hide();
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=true";
											else
												window.location.href = window.location.href + "?applied=true";
										}, function(err){
											console.log(err);
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=false";
											else
												window.location.href = window.location.href + "?applied=false";
										});
						}, function(err){
							companyService.addCandidate(candidate)
								.then(function(response){
									console.log(response);
									application.candidateId = response.id;
									companyService.apply(application)
										.then(function(res){
											console.log(res);
											// $(".details_apply_button").removeClass("disabled_button");
											// $(".body_block.right").hide();
											// $(".body_block.left").hide();
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=true";
											else
												window.location.href = window.location.href + "?applied=true";
										}, function(err){
											console.log(err);
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=false";
											else
												window.location.href = window.location.href + "?applied=false";
										});
								}, function(err){
									console.log(err);
								});
						});
					}, function(err){
						console.log(err);
					});
			});
	}	
}

function signUp(e){
	e.preventDefault();
	var passMatch = true;
	$(".validate-match").each((index, el) => {
		if((!$(el).find("input").val().trim().length || !$(el).next().find("input").val().trim().length) || (($(el).find("input").val().trim().length && $(el).next().find("input").val().trim().length) && $(el).find("input").val() !== $(el).next().find("input").val()))
			passMatch = false;
	});

	if(validateForm() && passMatch){
		$(".details_apply_button").addClass("disabled_button");
		application.jobId = JOB_INFO.id;
		application.jobCreatedBy = JOB_INFO.createdBy;
		application.companyId = COMPANY_INFO.id;
		user.name = application.name;
		user.email = application.email;
		user.language = "en";
		user.type = 2;
		user.timezoneOffset = (new Date()).getTimezoneOffset();
    	user.timezone = moment.tz.guess();
		user.password = $("input[name='password']").val();
		candidate = {};
		candidate.name = application.name;
		candidate.companyId = COMPANY_INFO.id;
		candidate.emails = [application.email];
		if(application.resume){
			candidate.resume = application.resume;
			candidate.resumeText = application.resumeText;
		}
		if(user.avatar)
			candidate.photo = user.avatar;

		companyService.isUserExists(user.email)
			.then(function(user){
				if(user.status == 5){
					companyService.register(user)
					.then(function(res){
						application.applierId = res.id;
						candidate.userId = res.id;
						companyService.isCandidateExists(COMPANY_INFO.id, application.email, res.id).then(function(response){
									console.log(response);
									application.candidateId = response.id;
									companyService.apply(application)
										.then(function(res){
											console.log(res);
											// $(".details_apply_button").removeClass("disabled_button");
											// $(".body_block.right").hide();
											// $(".body_block.left").hide();
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=true&registered=true";
											else
												window.location.href = window.location.href + "?applied=true&registered=true";
										}, function(err){
											console.log(err);
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=false&registered=true";
											else
												window.location.href = window.location.href + "?applied=false&registered=true";
										});
						}, function(err){
							companyService.addCandidate(candidate)
								.then(function(response){
									console.log(response);
									application.candidateId = response.id;
									companyService.apply(application)
										.then(function(res){
											console.log(res);
											// $(".details_apply_button").removeClass("disabled_button");
											// $(".body_block.right").hide();
											// $(".body_block.left").hide();
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=true&registered=true";
											else
												window.location.href = window.location.href + "?applied=true&registered=true";
										}, function(err){
											console.log(err);
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=false&registered=true";
											else
												window.location.href = window.location.href + "?applied=false&registered=true";
										});
								}, function(err){
									$(".details_apply_button").removeClass("disabled_button");
									console.log(err);
								});
						});
					}, function(err){
						$(".details_apply_button").removeClass("disabled_button");
						console.log(err);
					});
				} else {
					$(".details_apply_button").removeClass("disabled_button");
					$(".bem-blockquote").show();
					$(".validate-match").removeClass("error");
					$(".validate-match").next().removeClass("error");
				}

			}, function(err){
				console.log(err);
				companyService.register(user)
					.then(function(res){
						application.applierId = res.id;
						candidate.userId = res.id;
						companyService.isCandidateExists(COMPANY_INFO.id, application.email, res.id).then(function(response){
									console.log(response);
									application.candidateId = response.id;
									companyService.apply(application)
										.then(function(res){
											console.log(res);
											// $(".details_apply_button").removeClass("disabled_button");
											// $(".body_block.right").hide();
											// $(".body_block.left").hide();
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=true&registered=true";
											else
												window.location.href = window.location.href + "?applied=true&registered=true";
										}, function(err){
											console.log(err);
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=false&registered=true";
											else
												window.location.href = window.location.href + "?applied=false&registered=true";
										});
						}, function(err){
							companyService.addCandidate(candidate)
								.then(function(response){
									console.log(response);
									application.candidateId = response.id;
									companyService.apply(application)
										.then(function(res){
											console.log(res);
											// $(".details_apply_button").removeClass("disabled_button");
											// $(".body_block.right").hide();
											// $(".body_block.left").hide();
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=true&registered=true";
											else
												window.location.href = window.location.href + "?applied=true&registered=true";
										}, function(err){
											console.log(err);
											if(window.location.href.includes("?"))
												window.location.href = window.location.href + "&applied=false&registered=true";
											else
												window.location.href = window.location.href + "?applied=false&registered=true";
										});
								}, function(err){
									$(".details_apply_button").removeClass("disabled_button");
									console.log(err);
								});
						});
					}, function(err){
						$(".details_apply_button").removeClass("disabled_button");
						console.log(err);
					});
			});
	} else {
		$(".bem-blockquote").hide();
		$(".validate-match").addClass("error");
		$(".validate-match").next().addClass("error");
	}
}

function cancelSignUp(){
	$("#signupModal").find("input").val("");
}


function gotToJobPage(){
	var jobId = window.location.href.split("/")[window.location.href.split("/").length-2];
	window.location.href = window.location.protocol + "//" + window.location.hostname + "/company/job/" + jobId;
}

function loadPreviewImage(input){
	if (input.files && input.files[0]) {
        if(bytesToSize(input.files[0].size, 10) && (getExtension(input.files[0].name).toLowerCase() == 'jpg' || getExtension(input.files[0].name).toLowerCase() == 'jpeg' || getExtension(input.files[0].name).toLowerCase() == 'png')){
            $(input).parent().parent().parent().find(".loading-bar").show();
            companyService.uploadFile(input.files[0], function(percent){
                //console.log(percent);
                var p = Math.round(percent*100);
                $(input).parent().parent().parent().find(".loading-bar > div > div").css("width", (p+"%"));
                $(input).parent().parent().parent().find(".loading-bar > div > span").text(p + "%");
                if(p == 100){
                    $(input).parent().parent().parent().find(".loading-bar").hide();
                    $(input).parent().parent().parent().find(".processing-bar").show();
                }
            }).then(function(res){
                //console.log(res);
                $(input).parent().parent().parent().find(".processing-bar").hide();
                user.avatar = res.id;
                application.photo = res.id;
                $(".profile_photo_preview").css("background-image", "url(" + res.url + ")");
				$(input).parent().parent().next().removeClass("empty");
				validateForm();
            }, function(err){
                console.log(err);
            });
        } else if(!bytesToSize(input.files[0].size, 10)) {
			alert("Size of photo exceeds limit (10 mb).");
			$(input).parent().parent().next().addClass("empty");
			$(input).parent().parent().next().css("background-image", "");
			$(input).val("");
		} else {
			alert("File extension not supported");
			$(input).parent().parent().next().addClass("empty");
			$(input).parent().parent().next().css("background-image", "");
			$(input).val("");
		}
  }
}

function loadPreviewVideo(input, dur){
	if (input.files && input.files[0]) {

		if(getExtension(input.files[0].name).toLowerCase() != 'mp4' && getExtension(input.files[0].name).toLowerCase() != 'webm'){
				alert("File extension not supported");
				$(input).parent().parent().next().addClass("empty");
				$(input).parent().parent().next().find("video")[0].src = "";
				$(input).val("");
		} else {
        videoDuration(input.files[0], function(duration){
             var videoD = videoDurations[dur];
            //console.log("dur: ", duration, videoD);
			if(Math.floor(duration) <= videoD && bytesToSize(input.files[0].size, 50)){
                $(input).parent().parent().parent().find(".loading-bar").show();
                socket.emit("uploadVideo", window.localStorage.token);
                currentVideoUploadEl.type = "upload";
                currentVideoUploadEl.el = input;
                companyService.uploadVideo(input.files[0], function(percent){
                    console.log(percent);
                    var p = Math.round(percent*100);
                    $(input).parent().parent().parent().find(".loading-bar > div > div").css("width", (p+"%"));
                    $(input).parent().parent().parent().find(".loading-bar > div > span").text(p + "%");
                    if(p == 100){
                        $(input).parent().parent().parent().find(".loading-bar").hide();
                        $(input).parent().parent().parent().find(".processing-bar").show();
                    }
                }).then(function(res){
                    console.log(res);
                    $(input).parent().parent().parent().find(".processing-bar").hide();
                    $(input).parent().parent().next().find("video")[0].src = res.url;
				    $(input).parent().parent().next().removeClass("empty");
				    $(input).attr("data-id", res.id);
				    validateForm();
                }, function(err){
                    console.log(err);
                });
				
			} else if(Math.floor(duration) > videoD) {
				alert("Duration of video exceeds limit ("+dur+").");
				$(input).parent().parent().next().addClass("empty");
				$(input).parent().parent().next().find("video")[0].src = "";
				$(input).val("");
			} else {
                alert("Size of video exceeds limit 50 mb.");
				$(input).parent().parent().next().addClass("empty");
				$(input).parent().parent().next().find("video")[0].src = "";
				$(input).val("");
            }
        }); 
    }
  }
}

function loadResumeTitle(input){
	if (input.files && input.files[0]) {
        if(bytesToSize(input.files[0].size, 2) && getExtension(input.files[0].name).toLowerCase() == 'pdf'){
            $(input).parent().parent().parent().find(".loading-bar").show();
            companyService.uploadResume(input.files[0], function(percent){
                    //console.log(percent);
                    var p = Math.round(percent*100);
                    $(input).parent().parent().parent().find(".loading-bar > div > div").css("width", (p+"%"));
                    $(input).parent().parent().parent().find(".loading-bar > div > span").text(p + "%");
                    if(p == 100){
                        $(input).parent().parent().parent().find(".loading-bar").hide();
                        $(input).parent().parent().parent().find(".processing-bar").show();
                    }  
            }).then(function(res){
                //console.log(res);
                $(input).parent().parent().parent().find(".processing-bar").hide();
                $("#resume_title > p").text(input.files[0].name);
			    $(input).parent().parent().next().removeClass("empty");
			    application.resume = res.id;
			    application.resumeText = res.resume;
			    validateForm();
            }, function(err){
                console.log(err);
            });
        } else if(!bytesToSize(input.files[0].size, 2)) {
			alert("Size of file exceeds limit (2 mb).");
			$(input).parent().parent().next().addClass("empty");
			$(input).parent().parent().next().find("p").text("");
			$(input).val("");
		} else {
			alert("File extension not supported");
			$(input).parent().parent().next().addClass("empty");
			$(input).parent().parent().next().find("p").text("");
			$(input).val("");
		}
  }
}

function loadFileUpload(input){
	if (input.files && input.files[0]) {
        if(bytesToSize(input.files[0].size, 2) && supportedFileTypes.includes(getExtension(input.files[0].name).toLowerCase())){
            $(input).parent().parent().parent().find(".loading-bar").show();
            companyService.uploadFile(input.files[0], function(percent){
                    //console.log(percent);
                    var p = Math.round(percent*100);
                    $(input).parent().parent().parent().find(".loading-bar > div > div").css("width", (p+"%"));
                    $(input).parent().parent().parent().find(".loading-bar > div > span").text(p + "%");
                    if(p == 100){
                        $(input).parent().parent().parent().find(".loading-bar").hide();
                        $(input).parent().parent().parent().find(".processing-bar").show();
                    }  
            }).then(function(res){
                //console.log(res);
                $(input).parent().parent().parent().find(".processing-bar").hide();
                $(input).parent().parent().next().find("p").text(input.files[0].name);
			    $(input).parent().parent().next().removeClass("empty");
			    $(input).data("id", res.id);
			    validateForm();
            }, function(err){
                console.log(err);
            });
        } else if(!bytesToSize(input.files[0].size, 2)) {
			alert("Size of file exceeds limit (2 mb).");
			$(input).parent().parent().next().addClass("empty");
			$(input).parent().parent().next().find("p").text("");
			$(input).val("");
		} else {
			alert("File extension not supported");
			$(input).parent().parent().next().addClass("empty");
			$(input).parent().parent().next().find("p").text("");
			$(input).val("");
		}
  }
}

function removeProfilePhoto(el){
	//console.log($(el));
	application.photo = null;
	user.avatar = null;
	$(el).parent().addClass("empty");
	$(el).parent().css("background-image", "");
	$(el).parent().prev().find("input").val("");
	validateForm();
}

function removeVideoPreview(el){
	//console.log($(el));
	$(el).prev()[0].pause();
	$(el).prev()[0].removeAttribute('src'); 
	$(el).prev()[0].load();
	$(el).parent().addClass("empty");
	$(el).parent().css("background-image", "");
	$(el).parent().prev().find("input").val("");
	validateForm();
}

function removeResumeTitle(el){
	$(el).parent().addClass("empty");
	$(el).prev().text("");
	$(el).parent().prev().find("input").val("");
	validateForm();
}

function removeFileUpload(el){
	$(el).parent().addClass("empty");
	$(el).prev().text("");
	$(el).parent().prev().find("input").val("");
	validateForm();
}

function buildQuestionsTemplate(q){
	var temp = "";
	if(q.length){
		q.forEach((el, i) => {
			switch(el.type){
				case "multiChoice":
					temp += '<div class="question_choose';
					if(el.isRequired)
						temp += ' required';
					temp += '" data-index="'+i+'"><label>'+el.name;
					if(el.isRequired)
						temp += '*';
					temp += '</label><div class="checkbox_btns">';
					el.choices.forEach((c, j) => {
						temp += '<div class="checkbox_btn"><input type="checkbox" onchange="validateForm()" value="'+c.value+'" name="choice'+i+'"> <span>'+c.value+'</span></div>';
					});
					temp += "</div></div>";
					break;
				case "singleChoice":
					temp += '<div class="question_schoose';
					if(el.isRequired)
						temp += ' required';
					temp += '"><label>'+el.name;
					if(el.isRequired)
						temp += '*';
					temp += '</label><div class="radio_btns">';
					el.choices.forEach((c, j) => {
						temp += '<div class="radio_btn"><input type="radio" onchange="validateForm()" value="'+c.value+'" name="schoice'+i+'"> <span>'+c.value+'</span></div>';
					});
					temp += "</div></div>";
					break;
				case "imageChoice":
					temp += '<div id="'+el.id+'" class="question_schoose';
					if(el.isRequired)
						temp += ' required';
					temp += '"><label>'+el.name;
					if(el.isRequired)
						temp += '*';
					temp += '</label><div class="radio_btns">';
					let qId = el.id, imagesTemp = '', imagesCount = 0, imagesArray = [];
					el.choices.forEach((c, j) => {
						companyService.getFileUrl(c.value).then(function(res){
							imagesArray.push({index: j, url: res});
							imagesCount++;
							if(imagesCount == el.choices.length){
								imagesArray.sort(function(a,b){
									if(a.index > b.index)
										return 1;
									else if(a.index < b.index)
										return -1;
									else
										return 0;
								});
								imagesArray.forEach(function(image){
									imagesTemp += '<div class="radio_btn image_choice_btn"><input type="radio" onchange="validateForm()" value="'+image.url+'" name="schoice'+i+'"><div class="image_choice" onclick="showImageModal(\''+image.url+'\')"><img src="'+image.url+'" /></div></div>';
								});
								$("#"+qId).find(".radio_btns").append(imagesTemp);
							}
						}, function(err){
							console.log(err);
						});
					});
					temp += "</div></div>";
					break;
				case "yesNoQuestion":
					temp += '<div class="question_schoose';
					if(el.isRequired)
						temp += ' required';
					temp += '"><label>'+el.name;
					if(el.isRequired)
						temp += '*';
					temp += '</label><div class="radio_btns">';
					temp += '<div class="radio_btn"><input type="radio" onchange="validateForm()" value="yes" name="yesNo'+ i +'"> <span>Yes</span></div>';
					temp += '<div class="radio_btn"><input type="radio" onchange="validateForm()" value="no" name="yesNo'+ i +'"> <span>No</span></div>';
					temp += "</div></div>";
					break;
				case "textQuestion":
					temp += '<div class="question_text';
					if(el.isRequired)
						temp += ' required';
					temp += '"><label>'+el.name;
					if(el.isRequired)
						temp += '*';
					temp += '</label><div class="editor" style="margin-top:12px;">';
					temp += '<textarea placeholder="Type answer here" onchange="validateForm()"></textarea>';
					temp += "</div></div>";
					break;
				case "fileUpload":
					temp += '<div class="question_file';
					if(el.isRequired)
						temp += ' required';
					temp += '"><label>'+el.name;
					if(el.isRequired)
						temp += '*';
					temp += '</label>';
					temp += '<div class="uploading_block" style="margin-top: 12px;">' +
							'<button>Choose file <input type="file" onchange="loadFileUpload(this)"></button>' +
							'<span>max 2 mb</span></div>' +
							'<div class="file-upload-title-block empty">' +
							'<p></p> <i class="icon icon-bin2" onclick="removeFileUpload(this);"></i></div>' +
							'<div class="loading-bar"><div><div></div><span></span></div></div></div>';
					break;
				case "videoQuestion":
					temp += '<div class="question_video';
					if(el.isRequired)
						temp += ' required';
					temp += '"><label>'+el.name;
					if(el.isRequired)
						temp += '*';
                    var dur = "'" + el.videoDuration.toString() + "'";
					temp += '</label><div class="video_upload">';
					temp += '<button onclick="startVideoStream(this, '+dur+');">Record Video</button>' +
								'<button class="upload_button">Upload Video <input type="file" name="videoq'+i+'" accept=".mp4, .webm" onchange="loadPreviewVideo(this, '+dur+')" /></button>' +
								'<span>MP4, WEBM, max 50 mb, '+el.videoDuration+'</span>';
					temp +=     '</div><div class="video_preview empty"><video width="80%" height="auto" controls></video>' +
									'<i class="icon icon-bin2" onclick="removeVideoPreview(this);"></i></div>' + 
                                '<div class="loading-bar"><div><div></div><span></span></div></div>' +
                                '<div class="processing-bar"><div><p>Processing </p><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i></div></div></div>';
					break;
			}
		});
		$(".questions_section > .section_body").html(temp);
		$(".questions_section").show();
	} 
}

function bytesToSize(bytes, limit) {
    if (bytes == 0) return true;
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
		if(i == 2 && (bytes / Math.pow(1024, i)).toFixed(1) <= limit) 
			return true;
		else if(i == 2 && (bytes / Math.pow(1024, i)).toFixed(1) > limit)
			return false;
	 	else if(i < 2)
			return true;
		else 
			return false;
}

function videoDuration(file, callback){
		var video = document.createElement('video');
		video.preload = 'metadata';

		video.addEventListener('loadedmetadata', function(e){
			window.URL.revokeObjectURL(video.src);
			callback(video.duration);
		});

		video.src = URL.createObjectURL(file);
}

function JSON_to_URLEncoded(element,key,list){
  var list = list || [];
  if(typeof(element)=='object'){
    for (var idx in element)
      JSON_to_URLEncoded(element[idx],key?key+'['+idx+']':idx,list);
  } else {
    list.push(key+'='+encodeURIComponent(element));
  }
  return list.join('&');
}
	
socket.on("connected", function(){
  //console.log("connected");
  socket.emit("news", "datata");
});

socket.on('videoUploaded', function (data) {
    console.log("video uploaded: ", data);
    setTimeout(function(){
        if(currentVideoUploadEl.type == "upload"){
            if(!$(currentVideoUploadEl.el).parent().parent().next().find("video")[0].src.trim().length){
                $(currentVideoUploadEl.el).parent().parent().parent().find(".processing-bar").hide();
                $(currentVideoUploadEl.el).parent().parent().next().find("video")[0].src = data.url;
				$(currentVideoUploadEl.el).parent().parent().next().removeClass("empty");
            }
        } else {
            if(!$(currentVideoUploadEl.el).parent().next().find("video")[0].src.trim().length){
                $(currentVideoUploadEl.el).parent().parent().find(".processing-bar").hide();
                $(currentVideoUploadEl.el).parent().next().find("video")[0].src = data.url;
				$(currentVideoUploadEl.el).parent().next().removeClass("empty");
            }
        }
    }, 1000);
});

function validateForm(){
	var validated = true;
	$(".validate-input").each((index, el) => {
		if(!$(el).find("input").val().trim().length)
			validated = false;
		else{
			if($(el).find("input").attr("name") == 'firstName')
				application.name.first = $(el).find("input").val();
			else if($(el).find("input").attr("name") == 'lastName')
				application.name.last = $(el).find("input").val();
			else
				application[$(el).find("input").attr("name")] = $(el).find("input").val();
		}
	});

	$(".validate-email").each((index, el) => {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		if(!$(el).find("input").val().trim().length || !re.test(String($(el).find("input").val()).toLowerCase()))
			validated = false;
		else
			application.email = $(el).find("input").val().toLowerCase();
		
	});

	$(".validate-textarea").each((index, el) => {
		if(!$(el).find("textarea").val().trim().length)
			validated = false
		else
			application.coverLetter = $(el).find("textarea").val();
	});

	if($(".cover_letter_section").find("textarea").val().trim().length){
		application.coverLetter = $(".cover_letter_section").find("textarea").val();
	}

	$(".validate-file").each((index, el) => {
		if(!$(el).find("input[type='file']").val().trim().length)
			validated = false
	});

	$(".validate-questions > div").each((index, el) => {
		if($(el).hasClass("question_video")){
			console.log("rewq: ", $(el).find("input[type='file']"), $(el).find("input[type='file']").val());
			if($(el).hasClass("required") && (!$(el).find("video").attr('src') || !$(el).find("video").attr('src').trim().length))
				validated = false
			else if($(el).find("video").attr('src') && $(el).find("video").attr('src').trim().length) {
				var question = {
					id: questions[index].id,
					name: questions[index].name,
					type: questions[index].type,
					questionType: questions[index].questionType,
					attachment: $(el).find("input[type='file']").data("id")
				};
				application.questions[index] = question;
			}
		} else if($(el).hasClass("question_text")){
			if($(el).hasClass("required") && !$(el).find("textarea").val().trim().length)
				validated = false;
			else if($(el).find("textarea").val().trim().length){
				var question = {
					id: questions[index].id,
					name: questions[index].name,
					type: questions[index].type,
					questionType: questions[index].questionType,
					choices: questions[index].choices,
					textAnswer: $(el).find("textarea").val().trim()
				};
				application.questions[index] = question;
			}
		} else if($(el).hasClass("question_file")){
			if($(el).hasClass("required") && !$(el).find("input[type='file']").val().trim().length)
				validated = false;
			else if($(el).find("input[type='file']").val().trim().length){
				var question = {
					id: questions[index].id,
					name: questions[index].name,
					type: questions[index].type,
					questionType: questions[index].questionType,
					attachment: $(el).find("input[type='file']").data("id")
				};
				application.questions[index] = question;
			}
		} else {
			if($(el).hasClass("required") && !$(el).find("input:checked").length)
				validated = false;
			else if($(el).hasClass("required") && $(el).find("input:checked").length) {
				var question = {
					id: questions[index].id,
					name: questions[index].name,
					type: questions[index].type,
					questionType: questions[index].questionType,
					choices: questions[index].choices,
					answer: []
				};

				if(questions[index].score)
					question.score = questions[index].score;

				$(el).find("input").each((i, e) => {
					if(question.type !== 'yesNoQuestion'){
						let q = {
							id: question.choices[i].id,
							value: question.choices[i].value,
							selected: false
						};

						if($(e).prop("checked"))
							q.selected = true;

						question.answer[i] = q;
					} else {
						let q = {
							id: question.choices[0].id,
							value: question.choices[0].value,
							selected: false
						};

						if($(e).prop("checked") && $(e).val() == "yes"){
							q.selected = true;
							question.answer[0] = q;
						} else if($(e).prop("checked") && $(e).val() == "no") {
							q.selected = false;
							question.answer[0] = q;
						}				
					}
				});
				application.questions[index] = question;
			}
		}
	});

	if($("input[name='phoneNumber']") && $("input[name='phoneNumber']").val().trim().length)
		application.phoneNumber = $("input[name='phoneNumber']").val().trim();

	if(validated){
		$(".details_apply_button").removeClass("disabled_button");
	} else {
		$(".details_apply_button").addClass("disabled_button");

	}
 	console.log("valid: ", validated, application);
	return validated;
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function getExtension(name) {
	return name.split(".").pop();
}

function showImageModal(url){
	console.log("test");
	$("body").addClass("modal-open");
	$("html").addClass("modal-open");
	$(".modal-backdrop").show();
	$("#imageChoiceModal").find(".modal-window-text > img").attr("src", url);
	$("#imageChoiceModal").fadeIn({duration: 400, easing: "swing"});
}

socket.on('disconnected', function() {
    socket.emit('delUser', window.localStorage.token);
});