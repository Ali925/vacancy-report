function hasGetUserMedia() {
  return !!(navigator.mediaDevices &&
    navigator.mediaDevices.getUserMedia);
}

var video, videoStream, recorder, allChunks = [], currentVideoDuration = 0, currentVideoEl = null, currentVideoLimitDuration = null;

var companyService = CompanyServices();

function startVideoStream(el, dur){
	if (hasGetUserMedia()) {
	  // Good to go!
        
        currentVideoLimitDuration = dur;
        currentVideoDuration = videoDurations[dur];
        currentVideoEl = el;
        
        $(".record-timer > p").text(currentVideoDuration);
        
        const constraints = {
		  video: true,
		  audio: true
		};

		video = $("#videoRecordModal").find("video")[0];

		navigator.mediaDevices.getUserMedia(constraints).then((stream) => {
			console.log(stream);
			videoStream = stream;
			showModal('videoRecordModal');
			if (video.mozSrcObject !== undefined) { // FF18a
              video.mozSrcObject = stream;
            } else if (video.srcObject !== undefined) {
              video.srcObject = stream;
            } else { // FF16a, 17a
              video.src = stream;
            }
		}).catch((error) => {
			console.log(error);
			alert('Access to media was forbidden');
		});
	} else {
	  alert('Video recorder is not supported by your browser');
	}
}

function startRecording(){
	var options = {mimeType: 'video/webm;codecs=vp9'};

	recorder = new MediaRecorder(videoStream, options);

	allChunks = [];

	recorder.ondataavailable = function(e) {
		if (e.data.size > 0) 
	 	 allChunks.push(e.data);
	}

	recorder.start();
    $(".play-btn").hide();
    $(".pause-btn").css("display", "inline-block");
    $(".stop-btn").css("display", "inline-block");
    companyService.startTimer(currentVideoDuration, function(time){
        //console.log("t: ", time);
        currentVideoDuration = Math.round(time);
        if(currentVideoDuration == 0){
            stopRecording();
        } else
            $(".record-timer > p").text(Math.round(time));
    });
}

function resumeRecording(){
	recorder.resume();
    companyService.startTimer(currentVideoDuration, function(time){
        currentVideoDuration = Math.round(time);
        if(currentVideoDuration == 0){
            stopRecording();
        } else
            $(".record-timer > p").text(Math.round(time));
    });
    $(".pause-btn").css("display", "inline-block");
    $(".resume-btn").hide();
}

function pauseRecording(){
	recorder.pause();
    companyService.stopTimer();
    $(".pause-btn").hide();
    $(".resume-btn").css("display", "inline-block");
}

function stopRecording(){
	recorder.stop();
    companyService.stopTimer();
    $(".modal-close-btn").click();
    videoStream.getTracks() 
    .forEach( track => track.stop() ); 
	console.log("chunks", allChunks);
	setTimeout(function(){
		const fullBlob = new Blob(allChunks, {
		    type: 'video/webm'
		});
        
        var file = blobToFile(fullBlob);
        console.log(file);
        
        calculateMediaDuration(file).then((duration) => {
            console.log("dur: ", duration);
            var videoD = videoDurations[currentVideoLimitDuration];
			if(Math.floor(duration) <= videoD && bytesToSize(file.size, 50)){
                socket.emit("uploadVideo", window.localStorage.token);
                currentVideoUploadEl.type = "record";
                currentVideoUploadEl.el = currentVideoEl;
                $(currentVideoEl).parent().parent().find(".loading-bar").show();
                companyService.uploadVideo(file, function(percent){
                    console.log(percent);
                    var p = Math.round(percent*100);
                    $(currentVideoEl).parent().parent().find(".loading-bar > div > div").css("width", (p+"%"));
                    $(currentVideoEl).parent().parent().find(".loading-bar > div > span").text(p + "%");
                    if(p == 100){
                        $(currentVideoEl).parent().parent().find(".loading-bar").hide();
                        $(currentVideoEl).parent().parent().find(".processing-bar").show();
                    }
                }).then(function(res){
                    console.log("video: ", res);
                    $(currentVideoEl).parent().parent().find(".processing-bar").hide();
                    $(currentVideoEl).parent().next().find("video")[0].src = res.url;
				    $(currentVideoEl).parent().next().removeClass("empty");
                    $(currentVideoEl).next().find("input[type='file']").attr("data-id", res.id);
                    validateForm();
                }, function(err){
                    console.log(err);
                });
				
			} else if(Math.floor(duration) > videoD) {
				alert("Duration of video exceeds limit ("+currentVideoLimitDuration+").");
				$(currentVideoEl).parent().next().addClass("empty");
				$(currentVideoEl).parent().next().find("video")[0].src = "";
			} else {
                alert("Size of video exceeds limit 50 mb.");
				$(currentVideoEl).parent().next().addClass("empty");
				$(currentVideoEl).parent().next().find("video")[0].src = "";
            }
        });
        
	},500);
	
}

function cancelRecording(){
    //recorder.stop();
    companyService.stopTimer();
}

function blobToFile(theBlob){
    //A Blob() is almost a File() - it's just missing the two properties below which we will add
    theBlob.lastModifiedDate = new Date();
    theBlob.name = generateFileName();
    return theBlob;
}

function generateFileName(){
    return "video_record_" + JOB_INFO.id + "_" + (new Date()).getTime() + ".webm";
}

function calculateMediaDuration(file){
  return new Promise( (resolve,reject)=>{
    var video = document.createElement('video');
    video.preload = 'metadata';
    video.addEventListener('loadedmetadata', function(e){
      // set the mediaElement.currentTime  to a high value beyond its real duration
      video.currentTime = Number.MAX_SAFE_INTEGER;
      // listen to time position change
      video.addEventListener('timeupdate', function(e){
        video.addEventListener('timeupdate', function(e){});
        // setting player currentTime back to 0 can be buggy too, set it first to .1 sec
        video.currentTime = 0.1;
        video.currentTime = 0;
        // media.duration should now have its correct value, return it...
        window.URL.revokeObjectURL(video.src); 
        resolve(video.duration);
      });
    });
      
    video.src = URL.createObjectURL(file);
  });
}