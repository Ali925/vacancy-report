var activeCompanyJobs = [], map, contacts = JSON.parse(document.getElementById("contactsContent").dataset.info), headQ = contacts.locations[0], 
jobapi= "https://test.glorri.az/job-service/", tagapi="https://test.glorri.az/tag-service/", userapi = "https://test.glorri.az/user-service/",
fileapi="https://test.glorri.az/file-service/", talentapi="https://test.glorri.az/talent-service/", transapi = "https://test.glorri.az/translation-service/", 
slide = null, slide2 = null, markers = {}, translations = [];

console.log("contacts:" , contacts);

$(document).ready(function(){
	var companyId = $("#contentBlock").data("id"), benefits = $("#benefitsBlock").data("benefits"), 
	gallery = $("#galleryBlock").data("gallery"), galleryCount = gallery.length, benefitsCount = benefits.length, 
	jobsCount = 0, headquarter = {};
	
	$("#galleryCount").text(galleryCount);
	$("#galleryCount2").text(galleryCount);

	if(contacts.locations && contacts.locations.length && contacts.locations.findIndex(l => l.isHeadquarter) != -1)
		headquarter = contacts.locations.find(l => l.isHeadquarter);
	else
		headquarter = contacts.locations[0];

	if(headquarter && headquarter.address.trim().length){
		$("#headQAddress").text(headquarter.address);
	} else {
		$("#headQAddress").parent().hide();
	}

	var aboutText = document.getElementById('aboutText');
    
    if(aboutText.offsetHeight <= 118){
        $("#showButton").hide();
    } else {
    	$("#aboutText").removeClass("bem-box__content--text-full");
    }

    /* Scrollbar Active */
    $('.scrollbar-inner, .scrollbar-macosx').scrollbar({
        'autoScrollSize': true,
    });

    $("#showButton").click(function(){
    	if($("#aboutText").hasClass("bem-box__content--text-full")){
    		$("#aboutText").removeClass("bem-box__content--text-full");
    		$("#showButton > span").text("View more");
    		$("#showButton > i").removeClass("_icon-chevron-up").addClass("_icon-chevron-down");
    	} else {
    		$("#aboutText").addClass("bem-box__content--text-full");
    		$("#showButton > span").text("View less");
    		$("#showButton > i").removeClass("_icon-chevron-down").addClass("_icon-chevron-up");
    	}
    	
    });

    if(!$("#companyVideo").attr("src").trim().length){
    	$("#companyVideo").parent().parent().hide();
    }

    $.ajax({
    	method: 'POST',
    	url: userapi + 'auth/job-token/'+ companyId
    }).then(function(res){
    	window.localStorage.token = res;
    	$.ajax({
    		method: 'GET',
    		url: jobapi + "/company/"+companyId+"/jobs-external",
    		headers: {
                'X-Auth-Token': res
            }
    	})
    	.then((data) => {
    		console.log('jobs: ', data);
    		if(data.length)
    			getCompanyJobs(data);
    		else{
    			$("#currentOpenings").remove();
    			$("#activeJobsCount").text('0');
    		}
    	}, (err) => {
    		console.log('error: ', err);
    	});
    }, function(err){
    	console.log(err);
    });

    

    $.get(fileapi + "/file/"+$(".wallpaper")[0].dataset.info+"/url")
    	.then((data) => {
    		console.log("data: ", data, $(".wallpaper").find(".bem-photo__src"));
    		$(".wallpaper").find(".bem-photo__src").css("background-image", "url("+data+")");
    	}, (err) => {
    		console.log(err);
    	});

    $.get(fileapi + "/file/"+$(".logo")[0].dataset.info+"/url")
    	.then((data) => {
    		console.log("data: ", data, $(".logo").find(".bem-photo__src"));
    		$(".logo").find(".bem-photo__src").css("background-image", "url("+data+")");
    		$(".logo").find("span.icon").hide();
    	}, (err) => {
    		console.log(err);
    	});

    $.get(transapi+"/translations/en")
		.then(function(data){
			console.log("translations: ", data);
			translations = data;
			translations.forEach(t=>{
	           				if('industries.'+ $("#industryInfo").data("info") == t.key){
	           					$("#industryInfo").text(t.value);
	           				} else if($("#employeesInfo").data("info") == t.key){
	           					$("#employeesInfo").text(t.value);
	           				}
	         });
			
		}, function(err){
			console.log(err);
		});

	$.get(talentapi+"/company/"+companyId+"/followers")
		.then(function(data){
			console.log("followers: ", data);
			$("#followersCount").text(data);
		}, function(err){
			console.log(err);
		});
	var jobsTemp = '', jobsLength = 0, jobs = {};

	function getCompanyJobs(data){
		$("#activeJobsCount").text(data.length);
           	jobsCount = data.length;
           	data.forEach(el => {
           		jobs[el] = {};
           		$.get(jobapi + "job-external/"+el)
	           		.then(function(data){
	           			console.log("job: ", data);
	           			translations.forEach(t=>{
	           				if("countries."+data.country == t.key){
	           					data.country = t.value;
	           				} else if("cities."+data.city == t.key){
	           					data.city = t.value;
	           				} else if("job-types."+data.type == t.key){
	           					data.jobType = t.value;
	           				}
	           			});

	           			jobs[data.id] = data;

	           			jobsLength++;
	           			if(jobsLength == jobsCount){
	           				renderJobs(jobs);
	           				if(jobsLength < 2){
	           					$("#jobsContentBtn").remove();
	           				}
	           			}
	           		}, function(err){
	           			console.log(err);
	           		});
           	});
	}

	function renderJobs(jobs){
		let jobLength = 0, jobsTemp = '';
		for(let job in jobs){
			if(jobLength < 5){
				let data = jobs[job];
				var jobType = data.type ? data.type.substring(0, (data.type.length-1)) : '';
				var timeDiff = (new Date()).getTime() - (new Date(data.postedDate)).getTime(), time = '';

	           			if(timeDiff > (1000 * 3600 * 24 * 30 * 12)){
	           				time = Math.round(timeDiff / (1000 * 3600 * 24 * 30 * 12)).toString();
	           				if(time > 1)
	           					time += " years ago";
	           				else
	           					time = "a year ago"; 
	           			} else if(timeDiff > (1000 * 3600 * 24 * 30)){
	           				time = Math.round(timeDiff / (1000 * 3600 * 24 * 30)).toString();
	           				if(time > 1)
	           					time += " months ago";
	           				else
	           					time = "a month ago"; 
	           			} else if(timeDiff > (1000 * 3600 * 24)){
	           				time = Math.round(timeDiff / (1000 * 3600 * 24)).toString();
	           				if(time > 1)
	           					time += " days ago";
	           				else
	           					time = "a day ago"; 
	           			} else if(timeDiff > (1000 * 3600)){
	           				time = Math.round(timeDiff / (1000 * 3600)).toString();
	           				if(time > 1)
	           					time += " hours ago";
	           				else
	           					time = "an hour ago"; 
	           			} else if(timeDiff > (1000 * 60)){
	           				time = Math.round(timeDiff / (1000 * 60)).toString();
	           				if(time > 1)
	           					time += " minutes ago";
	           				else
	           					time = "a minute ago"; 
	           			} else{
	           				time = Math.round(timeDiff / (1000)).toString();
	           				if(time > 10)
	           					time += " seconds ago";
	           				else
	           					time = "just now"; 
	           			}
				jobsTemp += '<div class="bem-box__item"><div class="inner w-100"><a href="https://glorri.az/company/job/'+data.id+'" target="_blank" class="name">'+data.title+'</a>' +
		           						'<div class="text">'+data.city+' • '+time+'</div>' +
		           						'<div class="time"><button class="bem-button bem-button--round bem-button--green bem-button--small bem-button--'+jobType+'">' +
	                                    data.jobType + '</button></div></div></div>';
	        }
	        jobLength++;
		}

		$("#jobsContent").prepend(jobsTemp);
	}

	var benTemp = '', benLength = 0;
	console.log("benefits!!!: ", benefits , $("#benefitsBlock > p").text().trim().length);
	if(!benefits.length && !$("#benefitsBlock > p").text().trim().length)
		$("#benefitsBox").hide();
	benefits.forEach(el => {
		$.get(tagapi+"/benefit-tag/"+el.benefitTagId)
			.then(function(data){
				console.log("benefit: ", data);
				if(data){
					benTemp += '<div class="bem-benefits__item">'+data.name+'</div>';
					benLength++;
		
					if(benLength == benefitsCount){
						$("#benefitsList").html(benTemp);
					}
				}
			}, function(err){
				console.log(err);
			});
	});
	var galleryTemp = '', galleryLength = 0, galleries = {};
	if(!gallery.length)
		$("#galleryBlock").hide();
	gallery.forEach((id, index) => {
		galleries[id] = {};
		var fileId = id;
		$.get(fileapi+"/file/"+fileId + "/url")
			.then(function(data){
				console.log("file: ", data);	
				galleries[fileId] = data;
				if(galleryLength <= 5){
					galleryTemp += '<li class="col-md-4 mb-4" data-src="'+data+'"><div class="bem-gallery__item">' +
                               '<img class="photo" src="'+data+'"></div></li>';
				} else {
					galleryTemp += '<li class="col-md-4 mb-4 d-none" data-src="'+data+'"><div class="bem-gallery__item">' +
                               '<img class="photo" src="'+data+'"></div></li>';
				}
			
				galleryLength++;

				if(galleryLength == galleryCount){
					renderGallery(galleries);
					
					if(galleryLength <= 5){
						$("#showGallery").remove();
					}

				}
			}, function(err){
				console.log(err);
			});
	});
});

	function renderGallery(gallery){
		let galleryLength = 0, galleryTemp = '';
		for(let g in gallery){
			let data = gallery[g];
			if(galleryLength <= 5){
					galleryTemp += '<li class="col-md-4 mb-4" data-src="'+data+'"><div class="bem-gallery__item">' +
                               '<img class="photo" src="'+data+'"></div></li>';
				} else {
					galleryTemp += '<li class="col-md-4 mb-4 d-none" data-src="'+data+'"><div class="bem-gallery__item">' +
                               '<img class="photo" src="'+data+'"></div></li>';
				}
			galleryLength++;
		}
		$("#galleryContent").html(galleryTemp);
		$('#galleryContent').lightGallery();
	}

	$("#showGallery").click(function(){
		$("#galleryContent").find("li.d-none").removeClass('d-none');
		$(this).hide();
	});

      function initMap() {
      	console.log("init map", headQ);
      	if(headQ){
	        map = new google.maps.Map(document.getElementById('map'), {
	          center: contacts.locations[0].coords,
	          zoom: 1,
	          disableDefaultUI: true, 
	          zoomControl: true, 
	          fullscreenControl: true
	        });
	        var headIcon = new google.maps.MarkerImage(
			    '../../assets/icons/headquarter.png',
			    null, /* size is determined at runtime */
			    null, /* origin is 0,0 */
			    null, /* anchor is bottom center of the scaled image */
			    new google.maps.Size(60, 60)
			); 
			var markIcon = new google.maps.MarkerImage(
			    '../../assets/icons/marker.png',
			    null, /* size is determined at runtime */
			    null, /* origin is 0,0 */
			    null, /* anchor is bottom center of the scaled image */
			    new google.maps.Size(50, 50)
			);  
	    //     markers.marker0 = new google.maps.Marker({
			  //   map: map,
			  //   icon: headIcon,
			  //   position: headQ.location,
			  //   title: 'Headquarter'
			  // });

	        contacts.locations.forEach((c, index) => {
	        	let icon = c.isHeadquarter ? headIcon : markIcon;
	        	markers["marker"+index] = new google.maps.Marker({
					 map: map,
					 icon: icon,
					 position: c.coords,
					 title: '<p>'+c.address+'</p><br /><strong>'+c.description+"</strong>"
				});
	        });

	        let emailTemp = '', phoneTemp = '', faxTemp = '';
	        contacts.emails.forEach((e, index) => {
	        	if(!index)
	        		emailTemp += "<span class='lab float-right' style='display: block;width: 92%;'>"+e+"</span>";
	        	else
	        		emailTemp += "<span class='lab float-right mt-2' style='display: block;width: 92%;'>"+e+"</span>";
	        });
	        contacts.phones.forEach((p, index) => {
	        	if(!index)
	        		phoneTemp += "<span class='lab float-right' style='display: block;width: 92%;'>"+p+"</span>";
	        	else
	        		phoneTemp += "<span class='lab float-right mt-2' style='display: block;width: 92%;'>"+p+"</span>";
	        });
	        contacts.fax.forEach((f, index) => {
	        	if(!index)
	        		faxTemp += "<span class='lab float-right' style='display: block;width: 92%;'>"+f+"</span>";
	        	else
	        		faxTemp += "<span class='lab float-right mt-2' style='display: block;width: 92%;'>"+f+"</span>";
	        });
	        $("#contactEmail").append(emailTemp);
	        $("#contactPhone").append(phoneTemp);
	        $("#contactFax").append(faxTemp);
	        var temp = '';
	        contacts.socialLinks.forEach((s, i) => {
	        					if(s.url){
	        						temp += '<a href="'+s.url+'" target="_blank" class="s-link text-center ';

	        						if(s.url.includes('facebook'))
	        							temp += 'bgcolor--facebook"><span class="icon icon-facebook"></span></a>';
	        						else if(s.url.includes('twitter'))
	        							temp += 'bgcolor--twitter"><span class="icon icon-twitter"></span></a>';
	        						else if(s.url.includes('linkedin'))
	        							temp += 'bgcolor--linkedin"><span class="icon icon-linkedin"></span></a>';
	        						else 
	        							temp += 'bgcolor--edge"><span class="icon icon-edge"></span></a>';
	        					}

	        				
	        });

	        				if(temp){
	        					$("#contactSocial").html(temp);
	        					$("#contactSocial").parent().show()
	        				}
	        				else
	        					$("#contactSocial").parent().hide();	

	        for(let m in markers){
	        	var marker = markers[m];
	        	marker.addListener('click', function(event) {
	        		new google.maps.InfoWindow({
		         	 content: this.title
		        	}).open(map, this);
	        	});
	        }
        }
      }

function changeZoom(){
	map.setZoom(1);
}

function goToJobs() {
	var id = window.location.pathname.split("/")[3];
	window.location.pathname = "/company/jobs/" + id;
}