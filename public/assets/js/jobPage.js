var companyService = CompanyServices(), map, map2;

$(document).ready(function(){

	if(getParameterByName("origin") == 'Email' && getParameterByName("email") && getParameterByName("email").trim().length){
		$(".decline_button").show();
		$(".details_apply_decline_button").show();
	}

	if(JOB_INFO.status == 1 || JOB_INFO.status == 3 || JOB_INFO.status == 5){
		$(".body_block_details_apply").hide();
		$(".body_block_details_share").hide();
		$(".body_block_details_list").css("border-bottom", "none");
	} else if(JOB_INFO.status == 6){
		$(".share_button").hide();
	}

	if(JOB_INFO.status == 3){
		$(".job_status").text("closed");
	} else if(JOB_INFO.status == 1) {
		$(".job_status").text("drafted");
	} else if(JOB_INFO.status == 5){
		$(".job_status").text("archived");
	} else {
		$(".job_status").hide();
	}

	if(!JOB_INFO.locationCoords || (JOB_INFO.locationCoords.latitude == 0 && JOB_INFO.locationCoords.longitude == 0)){
		$(".body_block_details_location").hide();
		$(".body_block_details_location_block").hide();
	}


	companyService.getJobToken(JOB_INFO.id)
    .then(function(res){
		//console.log(res);
        window.localStorage.token = res;
        companyService.addView(JOB_INFO.id)
		.then(function(res){
			console.log(res);
		}, function(err){
			console.log(err);
		});
	}, function(err){
		console.log(err);
	});

	
	companyService.getAllCandidates(JOB_INFO.id)
	.then(function(res){
		//console.log(res);
		$(".job_counter:first-child > p").text(res);
	}, function(err){
		console.log(err);
	});
	
	companyService.getFileUrl(JOB_INFO.coverImage)
	.then(function(res){
		//console.log(res);
		$(".desktop-view .header_cover").css("background-image", "url(" + res + ")");
	}, function(err){
		console.log(err);
	});
	
	companyService.getFileUrl(COMPANY_INFO.logo)
	.then(function(res){
		//console.log(res);
		$(".company_logo").append("<img src='"+res+"' />");
	}, function(err){
		console.log(err);
	});
	
	console.log("type: ", JOB_INFO.type);
	companyService.translateService("job-types.", JOB_INFO.type)
	.then(function(res){
		console.log("types: ", res);
		$(".job_type > span").text(res.value);
	}, function(err){
		console.log(err);
		$(".job_type").hide();
	});

	companyService.translateService("cities.", JOB_INFO.city)
	.then(function(res){
		console.log("types: ", res);
		$(".job_city > span").text(res.value);
	}, function(err){
		console.log(err);
		$(".job_city").hide();
	});
	
	companyService.translateService("currencies.", JOB_INFO.paymentCurrency)
	.then(function(currency){
		//console.log(res);
		companyService.translateService("payment-method.", JOB_INFO.paymentMethod)
		.then(function(payment){
			//console.log(res);
			$(".salary").text(JOB_INFO.paymentMin + " - " + JOB_INFO.paymentMax + " " + currency.value + " / " + payment.value);
		}, function(err){
			$(".salary").parent().hide();
			console.log(err);
		});
	}, function(err){
		$(".salary").parent().hide();
		console.log(err);
	});
	
	companyService.translateService("experiences.", JOB_INFO.experience)
	.then(function(res){
		//console.log(res);
		$(".experience").text(res.value);
	}, function(err){
		$(".experience").parent().hide();
		console.log(err);
	});
	
	companyService.translateService("industries.", JOB_INFO.industry)
	.then(function(res){
		//console.log(res);
		$(".industry").text(res.value);
	}, function(err){
		console.log(err);
	});
	
	companyService.translateService("industries.", JOB_INFO.subIndustry)
	.then(function(res){
		//console.log(res);
		$(".category").text(res.value);
	}, function(err){
		$(".category").parent().hide();
		console.log(err);
	});
	
	companyService.translateService("educations.", JOB_INFO.education)
	.then(function(res){
		//console.log(res);
		$(".education").text(res.value);
	}, function(err){
		$(".education").parent().hide();
		console.log(err);
	});
	
	companyService.translateService("career-levels.", JOB_INFO.careerLevel)
	.then(function(res){
		//console.log(res);
		$(".career").text(res.value);
	}, function(err){
		$(".career").parent().hide();
		console.log(err);
	});
	
	companyService.formatDate(JOB_INFO.deadline)
	.then(function(res){
		//console.log(res);
		$(".deadline").text(res);
	}, function(err){
		$(".deadline").parent().hide();
		console.log(err);
	});
	
	companyService.dateDifference(JOB_INFO.postedDate)
	.then(function(res){
		//console.log(res);
		$(".job_time > span").text("Posted " + res);
	}, function(err){
		console.log(err);
	});
	
	var skillsTemp = "", skillCount = 0, skillsArray = {};
	JOB_INFO.jobSkills = JOB_INFO.jobSkills.filter(s => s.skillTagId != undefined);
	JOB_INFO.jobSkills.forEach((s, index) => {
		companyService.getSkill(s.skillTagId)
			.then(function(res){
			//console.log(res, index);
			skillsTemp = "";
			skillsTemp += "<div class='body_block_skill'>" +
										"<p>" + res.name + "</p>" +
										// "<span>" + s.skillLevel + "/5</span>" +
										"<div class='skill-boxes'>";
			for(var i = 1;i<=5;i++){
				if(s.skillLevel >= i){
					skillsTemp += "<span><i></i></span>";
				} else if(s.skillLevel - (i-1) > 0){
					skillsTemp += "<span><i style='width:"+((s.skillLevel - (i-1))*100)+"%'></i></span>";
				} else {
					skillsTemp += "<span><i style='width:0%'></i></span>";
				}
			}
			
			skillsTemp += "</div></div>";
			
			skillsArray[index] = skillsTemp;
			
			skillCount++;
			
			if(skillCount == JOB_INFO.jobSkills.length){
				buildSkillsTemplate(skillsArray);
			}
		}, function(err){
			console.log(err);
		});
	});
	if(!JOB_INFO.jobSkills.length){
		$(".techSkills").hide();
		$(".techSkills + div").hide();
	}

	var languagesTemp = "", languagesCount = 0, languagesArray = {};
	JOB_INFO.languageSkills = JOB_INFO.languageSkills.filter(s => s.languageTagId != undefined);
	JOB_INFO.languageSkills.forEach((s, index) => {
		companyService.getLanguage(s.languageTagId)
			.then(function(res){
			//console.log(res, index);
			languagesTemp = "";
			languagesTemp += "<div class='body_block_skill'>" +
										"<p>" + res.name + "</p>" +
										// "<span>" + s.skillLevel + "/5</span>" +
										"<div class='skill-boxes'>";
			for(var i = 1;i<=5;i++){
				if(s.skillLevel >= i){
					languagesTemp += "<span><i></i></span>";
				} else if(s.skillLevel - (i-1) > 0){
					languagesTemp += "<span><i style='width:"+((s.skillLevel - (i-1))*100)+"%'></i></span>";
				} else {
					languagesTemp += "<span><i style='width:0%'></i></span>";
				}
			}
			
			languagesTemp += "</div></div>";
			
			languagesArray[index] = languagesTemp;
			
			languagesCount++;
			
			if(languagesCount == JOB_INFO.languageSkills.length){
				buildLanguageTemplate(languagesArray);
			}
		}, function(err){
			console.log(err);
		});
	});
	if(!JOB_INFO.languageSkills.length){
		$(".langSkills").hide();
		$(".langSkills + div").hide();
	}

	var softTemp = "", softCount = 0, softArray = {};
	JOB_INFO.softSkills = JOB_INFO.softSkills.filter(s => s.softSkillTagId != undefined);
	JOB_INFO.softSkills.forEach((s, index) => {
		companyService.getSoft(s.softSkillTagId)
			.then(function(res){
			//console.log(res, index);
			softTemp = "";
			softTemp += "<div class='body_block_skill'>" +
										"<p>" + res.name + "</p>" +
										// "<span>" + s.skillLevel + "/5</span>" +
										"<div class='skill-boxes'>";
			for(var i = 1;i<=5;i++){
				if(s.skillLevel >= i){
					softTemp += "<span><i></i></span>";
				} else if(s.skillLevel - (i-1) > 0){
					softTemp += "<span><i style='width:"+((s.skillLevel - (i-1))*100)+"%'></i></span>";
				} else {
					softTemp += "<span><i style='width:0%'></i></span>";
				}
			}
			
			softTemp += "</div></div>";
			
			softArray[index] = softTemp;
			
			softCount++;
			
			if(softCount == JOB_INFO.softSkills.length){
				buildSoftTemplate(softArray);
			}
		}, function(err){
			console.log(err);
		});
	});
	if(!JOB_INFO.softSkills.length){
		$(".softSkills").hide();
		$(".softSkills + div").hide();
	}
	
	var benefitsTemp = "", benefitsCount = 0, benefitsArray = {};
	if(JOB_INFO.benefits && JOB_INFO.benefits.length){
		JOB_INFO.benefits.forEach((b, index) => {
			companyService.getBenefit(b.benefitTagId)
				.then(function(res){
				//console.log(res, index);
				benefitsTemp = "";
				benefitsTemp += "<span>" + res.name + "</span>";
				
				benefitsArray[index] = benefitsTemp;
				
				benefitsCount++;
				
				if(benefitsCount == JOB_INFO.benefits.length){
					buildBenefitsTemplate(benefitsArray);
				}
			}, function(err){
				console.log(err);
			});
		});
	} else {
		$(".body_block_benefits").hide();
	}
	
	
	$(".share_buttons > div").click(function(){
		var type = $(this).attr('class').substr(0, $(this).attr('class').length-4);
		companyService.shareService(type);
	});
	
	$(".details_apply_social_btns > .fb-btn").click(function(){
		FB.login(function(response) {
  		// handle the response
			console.log(response);
			if(response.authResponse)
              {
                  FB.api('/me?fields=email,first_name,last_name,picture', function(responseFromFB){                                               
										console.log(responseFromFB);
                  });
              }
              else
              {
                  console.log('The login failed because they were already logged in');
              }
			}, {scope: 'public_profile,email'});
	});
	
	$(".details_apply_social_btns > .ln-btn").click(function(){
		let client = new jso.JSO({
			providerID: "linkedin",
			response_type: "code",
			client_id: "779lm5lixvz91l",
			redirect_uri: "https://glorri.az", // The URL where you is redirected back, and where you perform run the callback() function.
			authorization: "https://www.linkedin.com/oauth/v2/authorization",
			scope: "r_fullprofile"
		});
		
		client.callback();
		
		client.setLoader(jso.Popup);
		
		client.getToken("http://localhost:3000/popupCallback.html")
			.then((token) => {
				console.log("I got the token: ", token)
			})
			.catch((err) => {
				console.error("Error from passive loader", err)
			})
		});
	
    $(".email_btn").click(function(){
        var url = window.location.href;
        window.open('mailto:?subject='+JOB_INFO.title+'&body='+window.location.href, "_blank", "width=800,height=800");
    });
});

function buildSkillsTemplate(arr){
	var temp = "";
	for(var i = 0;i<JOB_INFO.jobSkills.length;){
		if(i%3 == 0){
				temp += "<div class='body_block_skills'>";
		}
		
		temp += arr[i];
		
		i++;
		
		if(i%3 == 0 || i == JOB_INFO.jobSkills.length){
				temp += "</div>";
				if(i == JOB_INFO.jobSkills.length)
					$(".techSkills + div").append(temp);
		}
	}

	if(!JOB_INFO.jobSkills.length){
		$(".techSkills").hide();
		$(".techSkills + div").hide();
	}
}

function buildLanguageTemplate(arr){
	var temp = "";
	for(var i = 0;i<JOB_INFO.languageSkills.length;){
		if(i%3 == 0){
				temp += "<div class='body_block_skills'>";
		}
		
		temp += arr[i];
		
		i++;
		
		if(i%3 == 0 || i == JOB_INFO.languageSkills.length){
				temp += "</div>";
				if(i == JOB_INFO.languageSkills.length)
					$(".langSkills + div").append(temp);
		}
	}

	if(!JOB_INFO.languageSkills.length){
		$(".langSkills").hide();
		$(".langSkills + div").hide();
	}
}

function buildSoftTemplate(arr){
	var temp = "";
	for(var i = 0;i<JOB_INFO.softSkills.length;){
		if(i%3 == 0){
				temp += "<div class='body_block_skills'>";
		}
		
		temp += arr[i];
		
		i++;
		
		if(i%3 == 0 || i == JOB_INFO.softSkills.length){
				temp += "</div>";
				if(i == JOB_INFO.softSkills.length)
					$(".softSkills + div").append(temp);
		}
	}

	if(!JOB_INFO.softSkills.length){
		$(".softSkills").hide();
		$(".softSkills + div").hide();
	}
}

function buildBenefitsTemplate(arr){
	var temp = "";
	for(var i = 0;i<JOB_INFO.benefits.length;){		
		temp += arr[i];
		i++;
	
		if(i == JOB_INFO.benefits.length){
				$(".benefits").append(temp);
		}
	}
}

function goToApply(){
	if(window.location.href.includes("?"))
		window.location.href = window.location.href.split("?")[0] + "/apply" + "?" + window.location.href.split("?")[1];
	else
		window.location.href = window.location.href + "/apply";
}

function goToLoginApply(){
	window.location.href = "https://bem.glorri.az/job/"+JOB_INFO.id+"/apply";
}

function initMap() { 
      	if(JOB_INFO.locationCoords){
	        map = new google.maps.Map(document.getElementById('map'), {
	          center: {lat: JOB_INFO.locationCoords.latitude, lng: JOB_INFO.locationCoords.longitude},
	          zoom: 8,
	          disableDefaultUI: true, 
	          zoomControl: true, 
	          fullscreenControl: true
	        });

	        let marker = new google.maps.Marker({
					 map: map,
					 position: {lat: JOB_INFO.locationCoords.latitude, lng: JOB_INFO.locationCoords.longitude},
					 title: '<p>Job location</p><br /><strong>'+JOB_INFO.location+"</strong>"
				});

	        marker.addListener('click', function(event) {
	        		new google.maps.InfoWindow({
		         	 content: this.title
		        	}).open(map, this);
	        	});

	        map2 = new google.maps.Map(document.getElementById('map2'), {
	          center: {lat: JOB_INFO.locationCoords.latitude, lng: JOB_INFO.locationCoords.longitude},
	          zoom: 8,
	          disableDefaultUI: true, 
	          zoomControl: true, 
	          fullscreenControl: true
	        });

	        let marker2 = new google.maps.Marker({
					 map: map2,
					 position: {lat: JOB_INFO.locationCoords.latitude, lng: JOB_INFO.locationCoords.longitude},
					 title: '<p>Job location</p><br /><strong>'+JOB_INFO.location+"</strong>"
				});

	        marker2.addListener('click', function(event) {
	        		new google.maps.InfoWindow({
		         	 content: this.title
		        	}).open(map2, this);
	        	});
        }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function cancelDecline(){
	$("#signupModal").find("textarea").val("");
}

function declineRequest(){
	companyService.decline(getParameterByName("invitation"), $("#signupModal").find("textarea").val()).then(function(res) {
		$("#signupModal .modal-window-text form").hide();
		$("#signupModal .modal-window-header > p").hide();
		$("#signupModal .modal-window-text-sucess").show();
		$("#signupModal .modal-window-header > img").show();
	}, function(err) {
		console.log(err);
	});
}